module.exports = {
  extends: ['airbnb'],
  parser: 'babel-eslint',
  env: {
    browser: true
  },
  plugins: [],
  settings: {
    'import/resolver': {
      node: {
        paths: ['src']
      }
    }
  },
  rules: {
    'max-len': ['error', 120],
    'jsx-a11y/no-static-element-interactions': 0,
    'no-useless-escape': 0,
    'no-else-return': 0,
    'arrow-body-style': 0,
    'no-param-reassign': 0,
    'no-bitwise': 0,
    'no-underscore-dangle': 0,
    'import/prefer-default-export': 0,
    'no-restricted-globals': 0,
    'react/forbid-prop-types': 0,
    'react/no-this-in-sfc': 0,
    'no-prototype-builtins': 0,
    'react/jsx-no-bind': 0,
    'react/no-array-index-key': 0,
    'react/prefer-stateless-function': 0,
    'react/destructuring-assignment': 0,
    'react/require-default-props': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-unresolved': 2,
    'function-paren-newline': ['error', 'consistent'],
    'prefer-destructuring': 0,
    'object-curly-newline': ['error', { consistent: true }],
    'jsx-a11y/anchor-is-valid': ['error', {
      components: ['Link'],
      specialLink: ['to']
    }],
    'comma-dangle': 0,
    'newline-after-var': 2,
    'react/no-did-update-set-state': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'react/jsx-props-no-spreading': 0,
    'react/state-in-constructor': 0,
    'react/static-property-placement': 0,
    'no-use-before-define': ['error', { functions: false }],
    // TODO: some below rules needs to be enable and fixes linter errors
    'import/no-cycle': 2,
    'react/prop-types': 0,
    camelcase: 0,
    'react/jsx-filename-extension': 0,
    'react/no-find-dom-node': 0,
    'react/default-props-match-prop-types': 0,
    'no-case-declarations': 0,
    'no-shadow': 0,
    'no-tabs': 0,
    'no-mixed-spaces-and-tabs': 0,
    'react/jsx-indent': [false, 2, 'tab', { checkAttributes: true }],
    'react/jsx-indent-props': [true, 'tab'],
    'linebreak-style': ['error', 'unix']
  },
};
