import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';
import tokenReducer from './modules/login/TokenReducer';
import loginReducer from './modules/login/LoginReducer';
import conferenceReducer from './modules/conference/reducer';
import chatReducer from './modules/chat/reducer';
import accountReducer from './modules/account/reducer';
import notifierReducer from './modules/notifier/reducer';
import imageCapturesReducer from './modules/imageCapture/reducer';
import pastConferenceReducer from './modules/pastConference/reducer';
import serverConfigReducer from './modules/serverConfig/reducer';
import massagesReducer from './modules/messages/reducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  token: tokenReducer,
  login: loginReducer,
  conference: conferenceReducer,
  chat: chatReducer,
  account: accountReducer,
  form: formReducer,
  notifications: notifierReducer,
  imageCapture: imageCapturesReducer,
  pastConference: pastConferenceReducer,
  config: serverConfigReducer,
  messages: massagesReducer
});
