import { get } from 'lodash';

export const userNameSelector = ({ token }) => {
  const first_name = get(token, 'token.teledentUserInfo.first_name', '');
  const last_name = get(token, 'token.teledentUserInfo.last_name', '');

  return `${first_name} ${last_name}`;
};
