import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Paper from '@material-ui/core/Paper';
import CreateMessage from 'modules/messages/components/CreateMessage';
import ActivityFeed from 'modules/messages/components/ActivityFeed';
import ImageList from 'modules/imageCapture/components/imageList/index';
import PrivateChat from 'modules/messages/components/PrivateChat';
import CallRequestPopup from '../modules/messages/components/CallRequestPopup';

export class App extends Component {
  state = {
    open: false,
    // menuAnchorEl: null
  };

  // handleLogoutClick = () => {
  //   this.props.logoutRequest();
  // };

  handleToggle = () => {
    const { location: { pathname } } = this.props;
    const page = pathname.split('/')[1];

    if (page === 'conference') {
      this.setState((prevstate) => {
        this.props.openSidebar({ open: !prevstate.open });
        return { open: !prevstate.open };
      });
    }
  };

  // handleMenuOpen = (event) => {
  //   this.setMenuAnchorEl(event.currentTarget);
  // };

  // setMenuAnchorEl = (menuAnchorEl) => this.setState({ menuAnchorEl });

  // handleMenuClose = () => {
  //   this.setMenuAnchorEl(null);
  // };

  // handleNotificationClick = () => {
  //   const { toggleActivityFeed } = this.props;
  //
  //   toggleActivityFeed(true);
  // };

  render() {
    const {
      classes,
      children,
      //    onShowCreateMessage,
      // inboxNotificationsCount,
      // userName,
      lastRequest,
      videoCallDecline,
      videoCallAccept
    } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.container}>
        {/* <QuickCreateMenu */}
        {/*    anchorEl={menuAnchorEl} */}
        {/*    onClose={this.handleMenuClose} */}
        {/*    onShowCreateMessage={onShowCreateMessage} */}
        {/* /> */}

        <CreateMessage />

        <Drawer
            open={open}
            width={200}
            variant="persistent"
            anchor="left"
        >
          <Paper>
            <IconButton onClick={this.handleToggle}>
              <ChevronLeftIcon />
            </IconButton>
            <ImageList />
          </Paper>
        </Drawer>

        <ActivityFeed />
        <PrivateChat />
        {children}
        {lastRequest
        && (
            <CallRequestPopup
              videoCallDecline={videoCallDecline}
              videoCallAccept={videoCallAccept}
              request={lastRequest}
            />
        )}
      </div>
    );
  }
}
