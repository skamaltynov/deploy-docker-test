import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { inboxNotificationsCountSelector } from 'modules/messages/selectors';
import { logoutRequest } from 'modules/login/actions';
import { openSidebar } from 'modules/imageCapture/actions';
import { toggleActivityFeed, toggleCreateMessage } from 'modules/messages/actions';
import { userNameSelector } from './selectors';
import useStyles from './App-styles';
import { App } from './App';
import { lastRequestSelector } from '../modules/messages/selectors';
import { videoCallAccept, videoCallDecline } from '../modules/messages/actions';

const mapStateToProps = (state) => ({
  token: state.token,
  inboxNotificationsCount: inboxNotificationsCountSelector(state),
  userName: userNameSelector(state),
  lastRequest: lastRequestSelector(state)
});

const onShowCreateMessage = () => toggleCreateMessage(true);

export default withStyles(useStyles)(connect(mapStateToProps, {
  logoutRequest,
  openSidebar,
  videoCallAccept,
  videoCallDecline,
  onShowCreateMessage,
  toggleActivityFeed
})(App));
