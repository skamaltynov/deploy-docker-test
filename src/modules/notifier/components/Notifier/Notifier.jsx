import { Component } from 'react';

export class Notifier extends Component {
  displayed = []

  shouldComponentUpdate({ notifications: newSnacks = [] }) {
    const { closeSnackbar, removeSnackbar, notifications: currentSnacks } = this.props;

    if (!newSnacks.length) {
      this.displayed = [];
      return false;
    }
    let notExists = false;

    for (let i = 0; i < newSnacks.length; i += 1) {
      const newSnack = newSnacks[i];

      if (newSnack.dismissed) {
        closeSnackbar(newSnack.key);
        removeSnackbar(newSnack.key);
      }
      // eslint-disable-next-line
      if (notExists) continue;
      notExists = notExists || !currentSnacks.filter(({ key }) => newSnack.key === key).length;
    }
    return notExists;
  }

  componentDidUpdate() {
    const { enqueueSnackbar, removeSnackbar, notifications = [] } = this.props;

    notifications.forEach(({ key, message, options = {} }) => {
      if (this.displayed.includes(key)) return;
      enqueueSnackbar(message, {
        ...options,
        onClose: (event, reason, key) => {
          if (options.onClose) {
            options.onClose(event, reason, key);
          }
          removeSnackbar(key);
        }
      });
      this.storeDisplayed(key);
    });
  }

  storeDisplayed = (id) => {
    this.displayed = [...this.displayed, id];
  }

  render() {
    return null;
  }
}
