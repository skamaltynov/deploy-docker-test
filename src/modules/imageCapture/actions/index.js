import * as at from './actionTypes';

export const addNewImage = (data) => ({
  type: at.IMAGE_CAPTURE_ADD,
  payload: data
});
export const openSidebar = (payload) => ({
  type: at.OPEN_SIDEBAR,
  payload
});

export const loadMedia = (payload) => ({
  type: at.LOADMEDIA,
  payload
});
