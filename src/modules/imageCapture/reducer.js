import * as at from './actions/actionTypes';

const initialState = {
  images: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case at.LOADMEDIA: {
      const { appointmentId } = payload;

      return {
        ...state,
        images: {
          ...state.images,
          [appointmentId]: [
            ...payload.data],
        },
      };
    }

    // case at.IMAGE_CAPTURE_ADD: {
    //   const { appointmentId } = payload;
    //   const images = get(state.images, appointmentId, []);
    //
    //   return {
    //     ...state,
    //     images: {
    //       ...state.images,
    //       [appointmentId]: [
    //         ...images,
    //         payload],
    //     },
    //   };
    // }


    default:
      return state;
  }
};
