import { find, get } from 'lodash';

export const appointmentIdSelector = (state) => state.conference.janusConfig.appointmentId;
export const imageSelector = (state) => get(state.imageCapture.images, appointmentIdSelector(state), []);
export const tokenSelector = (state) => get(state, 'token.token.teledentApiToken', '');
export const defaultVideoDeviceSelector = (state) => get(state, 'conference.videoDevices.0');
export const dataFeedSelector = (state) => {
  const feeds = get(state, 'conference.feeds');

  return find(feeds,
    (feed) => {
      const data = get(feed, 'offer.media.data');

      return !feed.remote && data;
    });
};

export const accountIdSelector = ({ login: state }) => get(state, 'userInfo.id', '');
