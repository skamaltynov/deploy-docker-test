import { call, put, select, takeEvery } from '@redux-saga/core/effects';
import * as at from './actions/actionTypes';
import APImethods from '../../api';
import { enqueueSnackbar } from '../notifier/actions';
import { accountIdSelector } from './selectors';

// function* loadConferenceImages() {
//   const id = yield select(appointmentIdSelector);
//   const { data } = yield call([APImethods, 'get'], `/appointments/${id}/media`);
//
//   yield put(loadMedia({ data, appointmentId: id }));
// }

function* sendConferenceMedia({ payload: { blob } }) {
  const formData = new FormData();
  const accountId = yield select(accountIdSelector);
  const filename = `${accountId}-screenshot-${new Date().getTime()}`;

  formData.append('accountId', accountId);
  formData.append('file', blob, filename);
  yield call([APImethods, 'postFormData'], '/media/account', formData);

  yield put(enqueueSnackbar('Screenshot uploaded'));
}

export default function* imageCaptureSaga() {
  yield takeEvery(at.IMAGE_CAPTURE_ADD, sendConferenceMedia);
  // yield takeLatest([t.CREATE_SESSION, at.OPEN_SIDEBAR], loadConferenceImages);
}
