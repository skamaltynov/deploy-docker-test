import { call, put, select, take, takeEvery, takeLatest } from 'redux-saga/effects';
import { END, eventChannel } from 'redux-saga';
import { get } from 'lodash';
import { pushVideoConferenceNotification } from 'modules/messages/actions';
import { directVideoCallSelector } from 'modules/messages/selectors';
import * as at from './actions/actionTypes';
import * as a from './actions';
import {
  activeRoomSelector,
  isFutureConferenceSelector,
  isPastConferenceSelector,
  newAppointmentDataSelector
} from './selectors';
import APImethods from '../../api';
import history from '../../history';
import { showRingingModal } from '../messages/actions';
import { currentUserSelector } from '../messages/selectors';

function countdown(secs, timeout) {
  return eventChannel((emitter) => {
    const iv = setInterval(() => {
      secs -= 1;
      if (secs > 0) {
        emitter(secs);
      } else {
        // this causes the channel to close
        emitter(END);
      }
    }, timeout);
    // The subscriber must return an unsubscribe function

    return () => {
      clearInterval(iv);
    };
  });
}

export function* fetchNotificationsSaga() {
  const chan = yield call(countdown, 1440, 5000);

  try {
    while (true) {
      // take(END) will cause the saga to terminate by jumping to the finally block
      yield take(chan);

      const { data: notifications } = yield call([APImethods, 'get'], 'notifications/subscribe');

      yield put(a.setInboxNotification(notifications));

      const videoCall = yield select(directVideoCallSelector);
      const currentUser = yield select(currentUserSelector);

      if (get(videoCall, 'recipient_id') === currentUser.uid) {
        yield put(showRingingModal(videoCall));
      }
    }
  } catch (e) {
    const { code, error } = get(e, 'response.data');
    const errorMessage = code && error ? `${code} ${error}` : e;

    yield put(a.showNotification('error', errorMessage));
  } finally {
    // eslint-disable-next-line no-console
    console.log('countdown terminated');
  }
}

export function* fetchConferenceListSaga() {
  const chan = yield call(countdown, 120, 60000);

  try {
    while (true) {
      // take(END) will cause the saga to terminate by jumping to the finally block
      yield take(chan);

      const { data: appointmentsList } = yield call([APImethods, 'get'], 'Appointments');

      yield put(a.getAppointmentSuccess(appointmentsList));
    }
  } catch (e) {
    const { code, error } = get(e, 'response.data');
    const errorMessage = code && error ? `${code} ${error}` : e;

    yield put(a.showNotification('error', errorMessage));
  } finally {
    // eslint-disable-next-line no-console
    console.log('countdown terminated');
  }
}

function* getAppointmentsSaga() {
  yield put(a.resetNotification());
  yield put(a.setLoading(true));
  yield put(a.fetchConferenceList());

  try {
    const { data: appointmentsList } = yield call([APImethods, 'get'], 'Appointments');
    const { data: providersList } = yield call([APImethods, 'get'], 'teledentAccounts');
    const { data: patientsList } = yield call([APImethods, 'get'], 'patients');
    const { data: appointmentsTypes } = yield call([APImethods, 'get'], 'appointments/types');
    const { data: appointmentsDurations } = yield call([APImethods, 'get'], 'appointments/durations');
    const { data: locations } = yield call([APImethods, 'get'], 'locations');

    yield put(a.getAppointmentSuccess(appointmentsList));
    yield put(a.getProvidersSuccess(providersList));
    yield put(a.getPatientsSuccess(patientsList));
    yield put(a.getDurationsSuccess(appointmentsDurations));
    yield put(a.getLocationsSuccess(locations));
    yield put(a.getAppointmentTypesSuccess(appointmentsTypes));
  } catch (e) {
    const { code, error } = get(e, 'response.data');
    const errorMessage = code && error ? `${code} ${error}` : e;

    yield put(a.showNotification('error', errorMessage));
  } finally {
    yield put(a.resetData());
    yield put(a.setLoading(false));
  }
}

function* startConferenceHandler({ payload: { roomId, id } }) {
  yield put(a.setActiveRoom(roomId, id));

  const isFutureConference = yield select(isFutureConferenceSelector, id);
  const isPastConference = yield select(isPastConferenceSelector, id);

  if (isFutureConference) {
    yield put(a.toggleModal(true));
  } else if (isPastConference) {
    yield history.push(`/${id}/info`);
  } else {
    yield put(a.startConference());
  }
}

function* startConference() {
  const { roomId, id } = yield select(activeRoomSelector);

  yield history.push(`/conference/${roomId}/${id}`);
  yield put(a.toggleModal(false));
}

function* createAppointmentRequest() {
  yield put(a.resetNotification());
  const data = yield select(newAppointmentDataSelector);

  yield put(a.setLoading(true));

  try {
    const { data: createdAppointment } = yield call([APImethods, 'post'], 'Appointments', data);
    const successMessage = 'The appointment was successfully created!';

    yield put(a.showNotification('success', successMessage));

    const { data: appointmentsList } = yield call([APImethods, 'get'], 'Appointments');

    yield put(a.getAppointmentSuccess(appointmentsList));

    const newAppointmentData = appointmentsList.find(({ id }) => id === createdAppointment.id);

    yield put(pushVideoConferenceNotification(newAppointmentData));
  } catch (e) {
    const { code, error } = get(e, 'response.data');

    yield put(a.showNotification('error', `${code} ${error}`));
  } finally {
    yield put(a.resetData());
    yield put(a.setLoading(false));
  }
}

export default function* accountSaga() {
  yield takeLatest(at.GET_APPOINTMENTS, getAppointmentsSaga);
  yield takeLatest(at.HANDLE_CONFERENCE_START, startConferenceHandler);
  yield takeEvery(at.CREATE_APPOINTMENT, createAppointmentRequest);
  yield takeEvery(at.START_CONFERENCE, startConference);
  yield takeEvery(at.FETCH_CONFERENCE_LIST, fetchConferenceListSaga);
  yield takeEvery(at.FETCH_NOTIFICATION, fetchNotificationsSaga);
}
