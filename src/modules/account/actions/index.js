import * as at from './actionTypes';

export const getAppointments = () => ({
  type: at.GET_APPOINTMENTS,
});

export const getAppointmentSuccess = (appointmentsList) => ({
  type: at.GET_APPOINTMENTS_SUCCESS,
  payload: appointmentsList,
});

export const handleConferenceStart = (roomId, id) => ({
  type: at.HANDLE_CONFERENCE_START,
  payload: { roomId, id },
});

export const getProvidersSuccess = (providersList) => ({
  type: at.GET_PROVIDERS_SUCCESS,
  payload: providersList,
});

export const getPatientsSuccess = (patientsList) => ({
  type: at.GET_PATIENTS_SUCCESS,
  payload: patientsList,
});

export const getDurationsSuccess = (durations) => ({
  type: at.GET_DURATIONS_SUCCESS,
  payload: durations,
});

export const getLocationsSuccess = (locations) => ({
  type: at.GET_LOCATION_SUCCESS,
  payload: locations,
});

export const getAppointmentTypesSuccess = (types) => ({
  type: at.GET_APPOINTMENTS_TYPES_SUCCESS,
  payload: types,
});

export const setProvider = (provider) => ({
  type: at.SET_PROVIDER,
  payload: provider,
});

export const setPatient = (patient) => ({
  type: at.SET_PATIENT,
  payload: patient,
});


export const setDuration = (duration) => ({
  type: at.SET_DURATION,
  payload: duration,
});

export const setStartDate = (date) => ({
  type: at.SET_START_DATE,
  payload: date,
});

export const setLocation = (location) => ({
  type: at.SET_LOCATION,
  payload: location,
});

export const setAppointmentType = (type) => ({
  type: at.SET_APPOINTMENT_TYPE,
  payload: type,
});

export const createAppointment = () => ({
  type: at.CREATE_APPOINTMENT,
});

export const resetData = () => ({
  type: at.RESET_DATA,
});

export const setLoading = (loading) => ({
  type: at.SET_LOADING,
  payload: loading,
});

export const showNotification = (type, message) => ({
  type: at.SET_NOTIFICATION,
  payload: { type, message },
});

export const resetNotification = () => ({
  type: at.RESET_NOTIFICATION,
});

export const toggleModal = (state) => ({
  type: at.TOGGLE_MODAL,
  payload: state
});

export const setActiveRoom = (roomId, id) => ({
  type: at.SET_ACTIVE_ROOM,
  payload: { roomId, id }
});

export const startConference = () => ({
  type: at.START_CONFERENCE
});

export const fetchConferenceList = () => ({
  type: at.FETCH_CONFERENCE_LIST
});

export const getNotifications = () => ({
  type: at.FETCH_NOTIFICATION
});

export const setInboxNotification = (notifications) => ({
  type: at.SET_INBOX_NOTIFICATION,
  payload: notifications
});
