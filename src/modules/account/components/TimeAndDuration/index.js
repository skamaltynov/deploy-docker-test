import { connect } from 'react-redux';
import TimeAndDuration from './TimeAndDuration';
import * as s from '../../selectors';
import { setDuration, setStartDate } from '../../actions';

const mapStateToProps = (state) => ({
  durationOptions: s.durationOptionsSelector(state),
  selectedDuration: s.selectedDurationSelector(state),
  selectedDate: s.selectedDateDurationSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onDurationChange: (duration) => dispatch(setDuration(duration)),
  onDateChange: (duration) => dispatch(setStartDate(duration)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TimeAndDuration);
