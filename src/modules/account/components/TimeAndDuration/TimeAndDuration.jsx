import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import MomentUtils from '@date-io/moment';
import { KeyboardDatePicker, KeyboardTimePicker, MuiPickersUtilsProvider, } from '@material-ui/pickers';
import theme from './TimeAndDuration.module.css';

class TimeAndDuration extends Component {
    static propTypes = {
      selectedDuration: PropTypes.string.isRequired,
      onDurationChange: PropTypes.func.isRequired,
      onDateChange: PropTypes.func.isRequired,
      durationOptions: PropTypes.array.isRequired,
      selectedDate: PropTypes.object.isRequired,
    };

    handleDurationChange = (event) => {
      const { onDurationChange } = this.props;

      onDurationChange(event.target.value);
    };

    render() {
      const {
        durationOptions, selectedDuration, selectedDate, onDateChange,
      } = this.props;

      return (
        <div className={theme.container}>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
              margin="normal"
              id="date-picker-dialog"
              label="Select a date"
              format="MM/DD/YYYY"
              value={selectedDate}
              onChange={onDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
            <KeyboardTimePicker
              margin="normal"
              id="time-picker"
              label="Select start time"
              value={selectedDate}
              onChange={onDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change time',
              }}
            />
          </MuiPickersUtilsProvider>
          <FormControl className={theme.formControl}>
            <InputLabel>Duration</InputLabel>
            <Select
              value={selectedDuration}
              onChange={this.handleDurationChange}
            >
              {durationOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
            </Select>
          </FormControl>
        </div>
      );
    }
}

export default TimeAndDuration;
