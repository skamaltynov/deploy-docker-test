import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Participants from '../Participants';
import TimeAndDuration from '../TimeAndDuration';
import LocationAndType from '../LocationAndType';
import theme from './CreateAppointment.module.css';

class CreateAppointment extends Component {
    steps = ['Choose participants', 'Set start time and duration', 'Select type and location'];

    static propTypes = {
      onAppointmentCreate: PropTypes.func.isRequired,
    };

    state = {
      activeStep: 0,
    };


    get nextButtonDisabled() {
      const { isNewAppointmentDataValid } = this.props;
      const { activeStep } = this.state;

      return activeStep === 0 && !isNewAppointmentDataValid;
    }

    handleNext = () => {
      const { activeStep } = this.state;
      const { onAppointmentCreate } = this.props;

      if (activeStep === this.steps.length - 1) {
        onAppointmentCreate();
        this.handleReset();
      } else {
        this.setState((prevState) => ({ activeStep: prevState.activeStep + 1 }));
      }
    };

    handleBack = () => this.setState((prevState) => ({ activeStep: prevState.activeStep - 1 }));

    handleReset = () => {
      this.setState({ activeStep: 0 });
    };

    getStepContent = (step) => {
      switch (step) {
        case 0:
          return <Participants />;
        case 1:
          return <TimeAndDuration />;
        case 2:
          return <LocationAndType />;
        default:
          return 'Oooops';
      }
    };

    render() {
      const { activeStep } = this.state;

      return (
        <div className={theme.root}>
          <Typography variant="h4">Create new appointment</Typography>
          <Stepper activeStep={activeStep} orientation="vertical">
            {this.steps.map((label, index) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
                <StepContent>
                  {this.getStepContent(index)}
                  <div className={theme.actionsContainer}>
                    <Button
                      disabled={activeStep === 0}
                      onClick={this.handleBack}
                      className={theme.button}
                    >
                                        Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      disabled={this.nextButtonDisabled}
                      onClick={this.handleNext}
                      className={theme.button}
                    >
                      {activeStep === this.steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </div>
      );
    }
}

export default CreateAppointment;
