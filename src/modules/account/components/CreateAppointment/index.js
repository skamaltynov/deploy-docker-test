import { connect } from 'react-redux';
import CreateAppointment from './CreateAppointment';
import { createAppointment } from '../../actions';
import { isNewAppointmentDataValidSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  isNewAppointmentDataValid: isNewAppointmentDataValidSelector(state),
});
const mapDispatchToProps = (dispatch) => ({
  onAppointmentCreate: () => dispatch(createAppointment()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateAppointment);
