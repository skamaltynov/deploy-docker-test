import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import theme from './LocationAndType.module.css';

class LocationAndType extends Component {
    static propTypes = {
      selectedLocation: PropTypes.string.isRequired,
      selectedAppointmentType: PropTypes.string.isRequired,
      locationOptions: PropTypes.array.isRequired,
      appointmentsTypesOptions: PropTypes.array.isRequired,
      onLocationChange: PropTypes.func.isRequired,
      onAppointmentTypeChange: PropTypes.func.isRequired,
    };

    handleLocationChange = (event) => {
      const { onLocationChange } = this.props;

      onLocationChange(event.target.value);
    };

    handleAppointmentTypeChange = (event) => {
      const { onAppointmentTypeChange } = this.props;

      onAppointmentTypeChange(event.target.value);
    };

    render() {
      const {
        locationOptions,
        appointmentsTypesOptions,
        selectedLocation,
        selectedAppointmentType,
      } = this.props;

      return (
        <div className={theme.container}>
          <FormControl className={theme.formControl}>
            <InputLabel>Location</InputLabel>
            <Select
              value={selectedLocation}
              onChange={this.handleLocationChange}
            >
              {locationOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
            </Select>
          </FormControl>
          <FormControl className={theme.formControl}>
            <InputLabel htmlFor="age-simple">Type</InputLabel>
            <Select
              value={selectedAppointmentType}
              onChange={this.handleAppointmentTypeChange}
            >
              {appointmentsTypesOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
            </Select>
          </FormControl>
        </div>
      );
    }
}

export default LocationAndType;
