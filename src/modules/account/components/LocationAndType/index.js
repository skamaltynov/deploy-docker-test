import { connect } from 'react-redux';
import LocationAndType from './LocationAndType';
import * as s from '../../selectors';
import { setAppointmentType, setLocation } from '../../actions';

const mapStateToProps = (state) => ({
  selectedLocation: s.selectedLocationSelector(state),
  selectedAppointmentType: s.selectedAppointmentTypeSelector(state),
  locationOptions: s.locationsOptionsSelector(state),
  appointmentsTypesOptions: s.appointmentsTypesvOptionsSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onLocationChange: (location) => dispatch(setLocation(location)),
  onAppointmentTypeChange: (type) => dispatch(setAppointmentType(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationAndType);
