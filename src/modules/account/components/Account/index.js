import { connect } from 'react-redux';
import Account from './Account';
import * as a from '../../actions';
import { appointmentsListSelector, isModalOpenSelector, loadingSelector, notificationSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  appointmentsList: appointmentsListSelector(state),
  loading: loadingSelector(state),
  notification: notificationSelector(state),
  isModalOpen: isModalOpenSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  fetchAppointments: () => dispatch(a.getAppointments()),
  fetchNotification: () => dispatch(a.getNotifications()),
  onStartClick: (roomId, id) => dispatch(a.handleConferenceStart(roomId, id)),
  onModalClose: () => dispatch(a.toggleModal(false)),
  onModalConfirm: () => dispatch(a.startConference())
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);
