import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { isEqual, toNumber, trimEnd } from 'lodash';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { Typography } from '@material-ui/core';
import Loading from 'components/Loading';
import Notification from 'components/Notification';
import Modal from 'components/Modal';
import CreateAppointment from '../CreateAppointment';
import theme from './Account.module.css';

class Account extends Component {
  static propTypes = {
    fetchAppointments: PropTypes.func.isRequired,
    fetchNotification: PropTypes.func.isRequired,
    appointmentsList: PropTypes.array.isRequired,
    onStartClick: PropTypes.func.isRequired,
    notification: PropTypes.object,
    isModalOpen: PropTypes.bool.isRequired,
    onModalClose: PropTypes.func.isRequired,
    onModalConfirm: PropTypes.func.isRequired
  };

    static defaultProps = {
      notification: {},
    };

    state = {
      showNotification: false,
      appointmentsFilter: 'future'
    };

    componentDidMount() {
      const { fetchAppointments, fetchNotification } = this.props;

      fetchAppointments();
      fetchNotification();
    }

    componentDidUpdate(prevProps) {
      const { notification } = this.props;

      if (!isEqual(notification, prevProps.notification)) this.setState({ showNotification: true });
    }

    get futureConferenceList() {
      const { appointmentsList } = this.props;

      return appointmentsList.filter(({ appointment_start }) => moment(appointment_start).isAfter(moment()));
    }

    get pastConferenceList() {
      const { appointmentsList } = this.props;

      return appointmentsList.filter(({ appointment_start, duration }) => {
        const conferenceDuration = toNumber(trimEnd(duration, 'm'));

        return moment(appointment_start).add(conferenceDuration, 'minutes').isBefore(moment());
      });
    }

    get ongoingConferenceList() {
      const { appointmentsList } = this.props;

      return appointmentsList.filter(({ appointment_start, duration }) => {
        const start = moment(appointment_start);
        const now = moment();
        const conferenceDuration = toNumber(trimEnd(duration, 'm'));

        return start.isBefore(now) && start.add(conferenceDuration, 'minutes').isAfter(now);
      });
    }

    get appointmentsList() {
      const { appointmentsFilter } = this.state;

      switch (appointmentsFilter) {
        case 'future': return this.futureConferenceList;
        case 'past': return this.pastConferenceList;
        case 'ongoing': return this.ongoingConferenceList;
        default: return this.futureConferenceList;
      }
    }

    get appointmentsTitle() {
      const { appointmentsFilter } = this.state;

      switch (appointmentsFilter) {
        case 'future': return 'Future appointments';
        case 'past': return 'Past appointments';
        case 'ongoing': return 'Ongoing appointments';
        default: return 'Future appointments';
      }
    }

    get buttonTitle() {
      const { appointmentsFilter } = this.state;

      return appointmentsFilter === 'past' ? 'Info' : 'Start';
    }

    getStartTime = (appointmentStart) => moment(appointmentStart).format('DD/MM/YYYY [at] HH:mm');

    handleStartClick = (roomId, id) => {
      const { onStartClick } = this.props;

      onStartClick(roomId, id);
    };

    handleClose = () => {
      this.setState({ showNotification: false });
    };

    handleAppointmentsToggle = (event) => {
      this.setState({ appointmentsFilter: event.currentTarget.value });
    };

    render() {
      const { loading, notification: { type, message }, isModalOpen, onModalClose, onModalConfirm } = this.props;
      const { showNotification, appointmentsFilter } = this.state;

      if (loading) return <Loading />;

      return (
        <>
          {showNotification && <Notification type={type} message={message} onClose={this.handleClose} />}
          <CreateAppointment />
          <Divider />

          <Paper>
            <div className={theme.appointmentsHeader}>
              <Typography variant="h4">{this.appointmentsTitle}</Typography>
              <div className={theme.appointmentsSwitcher}>
                <FormControl component="fieldset">
                  <RadioGroup
                    aria-label="position"
                    name="position"
                    value={appointmentsFilter}
                    onChange={this.handleAppointmentsToggle}
                    row
                  >
                    <FormControlLabel
                      value="ongoing"
                      control={<Radio color="primary" />}
                      label="Ongoing"
                      labelPlacement="end"
                    />
                    <FormControlLabel
                      value="future"
                      control={<Radio color="primary" />}
                      label="Future"
                      labelPlacement="end"
                    />
                    <FormControlLabel
                      value="past"
                      control={<Radio color="primary" />}
                      label="Past"
                      labelPlacement="end"
                    />
                  </RadioGroup>
                </FormControl>
              </div>
            </div>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Start time</TableCell>
                  <TableCell align="right">Participants</TableCell>
                  <TableCell align="right">Appointment id</TableCell>
                  <TableCell align="right">Duration</TableCell>
                  <TableCell align="right">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.appointmentsList.map((appointment) => {
                  const { id, duration, appointment_start: appointmentStart, patient, provider, roomId } = appointment;

                  return (
                    <TableRow key={`${id}_${appointmentStart}`}>
                      <TableCell component="th" scope="row">
                        {this.getStartTime(appointmentStart)}
                      </TableCell>
                      <TableCell align="right">
                        <div>{`${provider.first_name} ${provider.last_name}`}</div>
                        <div>{`${patient.first_name} ${patient.last_name}`}</div>
                      </TableCell>
                      <TableCell align="right">{id}</TableCell>
                      <TableCell align="right">{duration}</TableCell>
                      <TableCell align="right">
                        <Button onClick={() => this.handleStartClick(roomId, id)}>{this.buttonTitle}</Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>

          <Modal
            open={isModalOpen}
            onClose={onModalClose}
            onConfirm={onModalConfirm}
            confirmTitle="Yes"
            cancelTitle="No"
          >
            <p>
              The conference is not started yet.
              Would you like to enter the conference room?
            </p>
          </Modal>
        </>
      );
    }
}

export default Account;
