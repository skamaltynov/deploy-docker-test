import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import theme from './Paticipants.module.css';

class Participants extends Component {
    static propTypes = {
      selectedProvider: PropTypes.string.isRequired,
      selectedPatient: PropTypes.string.isRequired,
      providersOptions: PropTypes.array.isRequired,
      patientsOptions: PropTypes.array.isRequired,
      onProviderChange: PropTypes.func.isRequired,
      onPatientChange: PropTypes.func.isRequired,
    };

    handleChangeProvider = (event) => {
      const { onProviderChange } = this.props;

      onProviderChange(event.target.value);
    };

    handleChangePatient = (event) => {
      const { onPatientChange } = this.props;

      onPatientChange(event.target.value);
    };

    render() {
      const {
        providersOptions,
        patientsOptions,
        selectedProvider,
        selectedPatient,
      } = this.props;

      return (
        <div className={theme.container}>
          <FormControl className={theme.formControl}>
            <InputLabel>Provider</InputLabel>
            <Select
              value={selectedProvider}
              onChange={this.handleChangeProvider}
            >
              {providersOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
            </Select>
          </FormControl>
          <FormControl className={theme.formControl}>
            <InputLabel htmlFor="age-simple">Patient</InputLabel>
            <Select
              value={selectedPatient}
              onChange={this.handleChangePatient}
            >
              {patientsOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
            </Select>
          </FormControl>
        </div>
      );
    }
}

export default Participants;
