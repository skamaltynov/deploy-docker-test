import { connect } from 'react-redux';
import Participants from './Participants';
import * as s from '../../selectors';
import { setPatient, setProvider } from '../../actions';

const mapStateToProps = (state) => ({
  providersOptions: s.providersOptionsSelector(state),
  patientsOptions: s.patientsOptionsSelector(state),
  selectedProvider: s.selectedProviderSelector(state),
  selectedPatient: s.selectedPatientSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onProviderChange: (provider) => dispatch(setProvider(provider)),
  onPatientChange: (patient) => dispatch(setPatient(patient)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Participants);
