import * as at from './actions/actionTypes';

const initialState = {
  appointmentsList: [],
  newAppointmentData: {
    appointment_start: new Date(),
    duration: '30m',
  },
  providersList: [],
  patientsList: [],
  appointmentsTypes: [],
  loading: false,
  notification: {},
  isModalOpen: false,
  activeRoom: {}
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case at.GET_APPOINTMENTS_SUCCESS:
      return {
        ...state,
        appointmentsList: payload,
      };

    case at.GET_PROVIDERS_SUCCESS:
      return {
        ...state,
        providersList: payload,
      };
    case at.GET_PATIENTS_SUCCESS:
      return {
        ...state,
        patientsList: payload,
      };

    case at.GET_DURATIONS_SUCCESS:
      return {
        ...state,
        durations: payload,
      };

    case at.GET_APPOINTMENTS_TYPES_SUCCESS:
      return {
        ...state,
        appointmentsTypes: payload,
      };

    case at.GET_LOCATION_SUCCESS:
      return {
        ...state,
        locations: payload,
      };

    case at.SET_PROVIDER:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          provider_id: payload,
        },
      };

    case at.SET_PATIENT:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          patient_id: payload,
        },
      };

    case at.SET_DURATION:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          duration: payload,
        },
      };

    case at.SET_START_DATE:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          appointment_start: payload,
        },
      };

    case at.SET_APPOINTMENT_TYPE:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          appointment_type: payload,
        },
      };

    case at.SET_LOCATION:
      return {
        ...state,
        newAppointmentData: {
          ...state.newAppointmentData,
          location_id: payload,
        },
      };

    case at.RESET_DATA:
      return {
        ...state,
        newAppointmentData: initialState.newAppointmentData,
      };

    case at.SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case at.SET_NOTIFICATION:
      return {
        ...state,
        notification: {
          type: payload.type,
          message: payload.message,
        },
      };

    case at.RESET_NOTIFICATION:
      return {
        ...state,
        notification: {},
      };

    case at.TOGGLE_MODAL:
      return {
        ...state,
        isModalOpen: payload,
      };

    case at.SET_ACTIVE_ROOM:
      const { roomId, id } = payload;

      return {
        ...state,
        activeRoom: { roomId, id },
      };

    default:
      return state;
  }
};
