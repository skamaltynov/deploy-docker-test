import { get, toNumber, trimEnd } from 'lodash';
import { createSelector } from 'reselect';
import moment from 'moment';

export const appointmentsListSelector = ({ account: state }) => get(state, 'appointmentsList', []);
export const providersListSelector = ({ account: state }) => get(state, 'providersList', []);
export const patientsListSelector = ({ account: state }) => get(state, 'patientsList', []);
export const durationsSelector = ({ account: state }) => get(state, 'durations', []);
export const locationsSelector = ({ account: state }) => get(state, 'locations', []);
export const appointmentsTypesSelector = ({ account: state }) => get(state, 'appointmentsTypes', []);

export const selectedProviderSelector = ({ account: state }) => get(state, 'newAppointmentData.provider_id', '');
export const selectedPatientSelector = ({ account: state }) => get(state, 'newAppointmentData.patient_id', '');
export const selectedDurationSelector = ({ account: state }) => get(state, 'newAppointmentData.duration', '');
export const selectedDateDurationSelector = ({ account: state }) => {
  return get(state, 'newAppointmentData.appointment_start', '');
};
export const selectedAppointmentTypeSelector = ({ account: state }) => {
  return get(state, 'newAppointmentData.appointment_type', '');
};
export const selectedLocationSelector = ({ account: state }) => get(state, 'newAppointmentData.location_id', '');
export const newAppointmentDataSelector = ({ account: state }) => get(state, 'newAppointmentData', {});
export const loadingSelector = ({ account: state }) => get(state, 'loading', false);
export const notificationSelector = ({ account: state }) => get(state, 'notification', {});

export const providersOptionsSelector = createSelector(
  providersListSelector,
  (providers) => providers.map(({ first_name, last_name, id }) => ({ key: id, label: `${first_name} ${last_name}` })),
);

export const patientsOptionsSelector = createSelector(
  patientsListSelector,
  (patients) => patients.map(({ first_name, last_name, id }) => ({ key: id, label: `${first_name} ${last_name}` })),
);

export const durationOptionsSelector = createSelector(
  durationsSelector,
  (durations) => durations.map((item) => ({ key: item, label: item })),
);

export const locationsOptionsSelector = createSelector(
  locationsSelector,
  (locations) => locations.map(({ id, name }) => ({ key: id, label: name })),
);

export const appointmentsTypesvOptionsSelector = createSelector(
  appointmentsTypesSelector,
  (types) => types.map((item) => ({ key: item, label: item })),
);

export const isNewAppointmentDataValidSelector = createSelector(
  selectedProviderSelector,
  selectedPatientSelector,
  (provider, patient) => Boolean(provider && patient),
);

export const isFutureConferenceSelector = createSelector(
  appointmentsListSelector,
  (_, appointmentId) => appointmentId,
  (appointmentsList, appointmentId) => {
    const { appointment_start } = appointmentsList.find(({ id }) => id === appointmentId);

    return moment(appointment_start).isAfter(moment());
  }
);

export const isPastConferenceSelector = createSelector(
  appointmentsListSelector,
  (_, appointmentId) => appointmentId,
  (appointmentsList, appointmentId) => {
    const { appointment_start, duration } = appointmentsList.find(({ id }) => id === appointmentId);
    const conferenceDuration = toNumber(trimEnd(duration, 'm'));

    return moment(appointment_start).add(conferenceDuration, 'minutes').isBefore(moment());
  }
);

export const isModalOpenSelector = ({ account: state }) => get(state, 'isModalOpen', false);
export const activeRoomSelector = ({ account: state }) => get(state, 'activeRoom', {});
