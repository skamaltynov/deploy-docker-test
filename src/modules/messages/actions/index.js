import * as at from './actionTypes';

export const toggleCreateMessage = (status) => ({
  type: at.TOGGLE_CREATE_MESSAGE,
  payload: status
});

export const setRecipient = (recipient) => ({
  type: at.SET_RECIPIENT,
  payload: recipient
});

export const sendPersonalMessage = (message, id) => ({
  type: at.SEND_PERSONAL_MESSAGE,
  payload: { message, id }
});

export const pushMessageNotification = (message, id) => ({
  type: at.PUSH_MESSAGE_NOTIFICATION,
  payload: { message, id }
});

export const deleteNotification = (id) => ({
  type: at.DELETE_NOTIFICATION,
  payload: id
});

export const pushVideoConferenceNotification = (data) => ({
  type: at.PUSH_VIDEO_CONFERENCING_NOTIFICATION,
  payload: data
});

export const toggleActivityFeed = (status) => ({
  type: at.TOGGLE_ACTIVITY_FEED,
  payload: status
});

export const setPersonalChatData = (data) => ({
  type: at.SET_PERSONAL_CHAT_DATA,
  payload: data
});

export const togglePrivateChat = (status) => ({
  type: at.TOGGLE_PRIVATE_CHAT,
  payload: status
});

export const setRecipientId = (id) => ({
  type: at.SET_RECIPIENT_ID,
  payload: id
});

export const fetchPersonalChatData = (id) => ({
  type: at.FETCH_PERSONAL_CHAT_DATA,
  payload: id
});

export const openPrivateChat = (id) => ({
  type: at.OPEN_PRIVATE_CHAT,
  payload: id
});

export const makeDirectVideoCall = (id) => ({
  type: at.MAKE_DIRECT_VIDEO_CALL,
  payload: id
});

export const showRingingModal = (videoCall) => ({
  type: at.DIRECT_VIDEO_CALL_INCOMING_REQUEST,
  payload: videoCall
});

export const setRecipientsList = (recipientsList) => ({
  type: at.SET_RECIPIENTS_LIST,
  payload: recipientsList
});

export const fetchRecipientsList = () => ({
  type: at.FETCH_RECIPIENTS_LIST
});

export const videoCallAccept = (request) => ({
  type: at.DIRECT_VIDEO_CALL_ACCEPT,
  payload: request
});

export const videoCallDecline = (request) => ({
  type: at.DIRECT_VIDEO_CALL_DECLINE,
  payload: request
});

export const setProviderToProvider = (status) => ({
  type: at.SET_PROVIDER_TO_PROVIDER,
  payload: status
});
