import { connect } from 'react-redux';
import InboxNotifications from './InboxNotifications';
import { inboxNotificationsSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  notificationsList: inboxNotificationsSelector(state)
});

const mapDispatchToProps = () => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(InboxNotifications);
