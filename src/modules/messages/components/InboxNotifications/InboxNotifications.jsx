import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Notification from '../Notification';
import theme from './InboxNotifications.module.css';

class InboxNotifications extends Component {
	static propTypes = {
	  notificationsList: PropTypes.array.isRequired
	};

	render() {
  	const { notificationsList } = this.props;

	  return (
			  <div className={theme.container}>
				  {notificationsList.map((notification, index) => (
						  <Notification data={notification} key={index} />
				  ))}
			  </div>
	  );
	}
}

export default InboxNotifications;
