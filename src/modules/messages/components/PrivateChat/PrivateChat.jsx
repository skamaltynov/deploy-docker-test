import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import Paper from '@material-ui/core/Paper';
import RefactoredChat from 'modules/messages/components/RefactoredChat';

class PrivateChat extends Component {
  render() {
  	const { onClose, isOpen, user, messages, recipientId, onMessageSend, loadChat } = this.props;

	  return (
		    <Drawer
				    open={isOpen}
				    anchor="right"
				    onClose={onClose}
		    >
			    <Paper>
				    <RefactoredChat
						    onClose={onClose}
						    user={user}
						    messages={messages}
						    appointmentId={recipientId}
						    onMessageSend={onMessageSend}
						    loadChat={loadChat}
				    />
			    </Paper>
		    </Drawer>
	  );
  }
}

export default PrivateChat;
