import { connect } from 'react-redux';
import PrivateChat from './PrivateChat';
import * as a from '../../actions';
import * as s from '../../selectors';

const mapStateToProps = (state) => ({
  isOpen: s.isPrivateChatOpenSelector(state),
  user: s.currentUserSelector(state),
  messages: s.privateMessagesSelector(state),
  recipientId: s.recipientIdSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  onClose: () => dispatch(a.togglePrivateChat(false)),
  onMessageSend: (message, recipientId) => dispatch(a.sendPersonalMessage(message, recipientId)),
  loadChat: (recipientId) => dispatch(a.fetchPersonalChatData(recipientId))
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivateChat);
