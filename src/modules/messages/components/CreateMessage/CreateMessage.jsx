import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import Modal from '../../../../components/Modal/Modal';
import theme from './CreateMessage.module.css';

class CreateMessage extends Component {
	static propTypes = {
	  onMessageSend: PropTypes.func.isRequired
	};

	state = {
	  message: ''
	};

	componentDidMount() {
	  const { fetchRecipients } = this.props;

	  fetchRecipients();
	}

	handleSendMessage = () => {
	  const { onMessageSend, onModalClose, selectedRecipient } = this.props;
	  const { message } = this.state;

	  onMessageSend(message, selectedRecipient);
	  onModalClose();
	};

	handleChangeProvider = (event) => {
	  const { onRecipientChange } = this.props;

	  onRecipientChange(event.target.value);
	};

	handleGoToChat = () => {
	  const { onModalClose, onPrivateChatOpen, selectedRecipient } = this.props;

	  onPrivateChatOpen(selectedRecipient);
	  onModalClose();
	};

	handleDirectVideoCall = () => {
	  const { onDirectVideoCall, selectedRecipient, onModalClose } = this.props;

	  onDirectVideoCall(selectedRecipient);
	  onModalClose();
	};

	render() {
	  const { isCreateMessageOpen, onModalClose, recipientsOptions, selectedRecipient } = this.props;
	  const { message } = this.state;

	  return (
  <div className={theme.container}>
    <Modal
      open={isCreateMessageOpen}
      onClose={onModalClose}
      onConfirm={this.handleSendMessage}
      confirmTitle="Send"
      cancelTitle="Cancel"
    >
      <div className={theme.modalContainer}>
        <Typography variant="h4">Create message</Typography>

        <FormControl className={theme.formControl}>
          <InputLabel>To</InputLabel>
          <Select
            value={selectedRecipient}
            onChange={this.handleChangeProvider}
          >
            {recipientsOptions.map(({ key, label }) => <MenuItem key={key} value={key}>{label}</MenuItem>)}
          </Select>
        </FormControl>

	      <TextareaAutosize
			      className={theme.textarea}
			      rows={8}
			      rowsMax={24}
			      placeholder="Write your message here"
			      onChange={(event) => this.setState({ message: event.currentTarget.value })}
			      value={message}
	      />

	     <div className={theme.buttonContainer}>
		     <Button
				     variant="contained"
				     className={theme.button}
				     onClick={this.handleGoToChat}
				     disabled={!selectedRecipient}
		     >
			     GO to chat
		     </Button>
		     <IconButton
				     color="inherit"
				     onClick={this.handleDirectVideoCall}
				     disabled={!selectedRecipient}
		     >
			     <VideoCallIcon className={theme.icon} />
		     </IconButton>
	     </div>

      </div>
    </Modal>
  </div>
	  );
	}
}

export default CreateMessage;
