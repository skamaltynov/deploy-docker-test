import { connect } from 'react-redux';
import CreateMessage from './CreateMessage';
import * as a from '../../actions';
import * as s from '../../selectors';

const mapStateToProps = (state) => ({
  isCreateMessageOpen: s.isCreateMessageOpenSelector(state),
  recipientsOptions: s.recipientsOptionsSelector(state),
  selectedRecipient: s.selectedRecipientSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onModalClose: () => dispatch(a.toggleCreateMessage(false)),
  onRecipientChange: (recipient) => dispatch(a.setRecipient(recipient)),
  onMessageSend: (value, id) => dispatch(a.sendPersonalMessage(value, id)),
  onPrivateChatOpen: (id) => dispatch(a.togglePrivateChat(true, id)),
  onDirectVideoCall: (id) => dispatch(a.makeDirectVideoCall(id)),
  fetchRecipients: () => dispatch(a.fetchRecipientsList())
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateMessage);
