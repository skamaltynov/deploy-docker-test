import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isEqual } from 'lodash';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import theme from './RefactoredChat.module.css';

class RefactoredChat extends Component {
	static propTypes = {
	  onMessageSend: PropTypes.func.isRequired,
	  messages: PropTypes.array.isRequired,
	  user: PropTypes.object,
	  onClose: PropTypes.func.isRequired,
	  appointmentId: PropTypes.string.isRequired,
	};

	constructor(props) {
	  super(props);

	  this.chatBody = React.createRef();
	  this.interval = null;
	}

	componentDidMount() {
	  const { loadChat, appointmentId } = this.props;

	  if (appointmentId) {
	    loadChat(appointmentId);

	    this.interval = setInterval(() => {
	      loadChat(appointmentId);
	    }, 5000);
	  }
	  this.scrollBottom();
	}

	componentDidUpdate(prevProps) {
	  const { messages } = this.props;

	  if (!isEqual(prevProps.messages, messages)) this.scrollBottom();
	}

	componentWillUnmount() {
	  clearInterval(this.interval);
	}

	sendMessageHandler = (event) => {
	  const { onMessageSend, appointmentId } = this.props;
	  const { value } = this.input;

	  if (!value) return;

	  if (event.keyCode === 13) {
	    onMessageSend(value, appointmentId);
	    this.input.value = '';
	  }
	};

	scrollBottom = () => {
	  this.chatBody.current.scrollTop = this.chatBody.current.scrollHeight;
	};

	isRemoteUser = (messageUser) => {
	  const { user } = this.props;

	  return user.uid !== messageUser.id;
	};

	userName = ({ first_name, last_name }) => `${first_name} ${last_name}`;

	time = (timeStamp) => moment(timeStamp).format('HH:mm');

	render() {
	  const { messages, onClose } = this.props;

	  return (
			  <div className={theme.container}>
				  <div className={theme.header}>
					  <h3>Private messages</h3>
					  <IconButton onClick={onClose}>
						  <CloseIcon />
					  </IconButton>
				  </div>

				  <div className={theme.body} ref={this.chatBody}>
					  {messages.map(({ body, created_at, created_by, id }) => (
							  <div
									  className={theme.message}
									  key={id}
									  style={{ alignItems: !this.isRemoteUser(created_by) ? 'flex-end' : '' }}
							  >
								  <div className={theme.messageHeader}>
									  {this.isRemoteUser(created_by)
									    && <div className={theme.author}>{`${this.userName(created_by)},`}</div>}
									  <div className={theme.date}>{this.time(created_at)}</div>
								  </div>
								  <div className={theme.messageBody}>{body}</div>
							  </div>
					  ))}
				  </div>

				  <Box className={theme.textComposer}>
					  <TextField
							  placeholder="Write a message..."
							  fullWidth
							  margin="normal"
							  variant="filled"
							  InputLabelProps={{ shrink: true }}
							  inputRef={(input) => { this.input = input; }}
							  onKeyDown={this.sendMessageHandler}
							  className={theme.textField}
					  />
				  </Box>
			  </div>
	  );
	}
}

export default RefactoredChat;
