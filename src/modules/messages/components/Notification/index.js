import { connect } from 'react-redux';
import { handleConferenceStart } from 'modules/account/actions';
import Notification from './Notification';
import * as a from '../../actions';
import { tokenSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  token: tokenSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  onDeleteNotification: (id) => dispatch(a.deleteNotification(id)),
  startConference: (roomId, id) => dispatch(handleConferenceStart(roomId, id)),
  onActivityFeedClose: () => dispatch(a.toggleActivityFeed(false)),
  onPrivateChatOpen: (id) => dispatch(a.openPrivateChat(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
