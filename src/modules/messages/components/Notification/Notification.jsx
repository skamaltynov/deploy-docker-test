import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { get } from 'lodash';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Avatar from '@material-ui/core/Avatar';
import theme from './Notification.module.css';

class Notification extends Component {
	static propTypes = {
	  data: PropTypes.object.isRequired,
	  onDeleteNotification: PropTypes.func.isRequired
	};

	get message() {
	  const { data } = this.props;

	  return data.payload.message || data.body;
	}

	get date() {
	  const { data: { created_at } } = this.props;

	  return moment(created_at).format('MMMM Do [,] YYYY HH:mm A');
	}

	get userName() {
	  const { data: { created_by: { first_name, last_name } } } = this.props;

	  return `${first_name} ${last_name}`;
	}

	get notificationType() {
	  const { data } = this.props;

	  return data.payload.type || data.type;
	}

	get avatarUrl() {
	  const { data, token } = this.props;
	  const url = get(data, ['created_by', 'media', 0, 'url'], '');

	  return `${url}?authToken=${token}`;
	}

	handleDeleteNotification = () => {
	  const { data: { id }, onDeleteNotification } = this.props;

	  onDeleteNotification(id);
	};

	handleGoToConference = () => {
	  const {
	  	data: { payload: { conferenceData: { roomId, id } } },
		  startConference,
		  onActivityFeedClose
	  } = this.props;

	  startConference(roomId, id);
	  onActivityFeedClose();
	  this.handleDeleteNotification();
	};

	handlePrivateChatOpen = () => {
	  const { data: { created_by: { id } }, onPrivateChatOpen, onActivityFeedClose } = this.props;

	  onPrivateChatOpen(id);
	  onActivityFeedClose();
	  this.handleDeleteNotification();
	};

	MessageNotification = () => {
	  return this.notificationType === 'private_message' && (
				<div className={theme.nameAndMessage} onClick={this.handlePrivateChatOpen}>
					<div>{this.userName}</div>
					<span className={theme.message}>{this.message}</span>
				</div>
	  );
	};

	VideoConferenceNotification = () => {
	  return this.notificationType === 'Video Conferencing' && (
				<div>
					{/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
					<a href="#" onClick={this.handleGoToConference}>
						<div>{this.userName}</div>
						has sent you a video conference request.
					</a>
				</div>
	  );
	};

	render() {
	  const { data } = this.props;
  	const { MessageNotification, VideoConferenceNotification } = this;

	  return (
			  <div className={theme.container}>
				  <Avatar src={this.avatarUrl} className={theme.avatar} />

				  <div className={theme.body}>
							<MessageNotification />

							<VideoConferenceNotification props={data} />
					  <div>
						  <span className={theme.date}>{this.date}</span>
						  <IconButton onClick={this.handleDeleteNotification}>
							  <DeleteIcon className={theme.icon} />
						  </IconButton>
					  </div>
				  </div>
			  </div>
	  );
	}
}

export default Notification;
