import { connect } from 'react-redux';
import ActivityFeed from './ActivityFeed';
import { toggleActivityFeed } from '../../actions';
import { isActivityFeedOpenSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  isActivityFeedOpen: isActivityFeedOpenSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  handleClose: () => dispatch(toggleActivityFeed(false))
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityFeed);
