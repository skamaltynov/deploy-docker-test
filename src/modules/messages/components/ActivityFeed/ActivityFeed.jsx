import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Drawer from '@material-ui/core/Drawer';
import InboxNotifications from '../InboxNotifications';
import theme from './ActivityFeed.module.css';

class ActivityFeed extends Component {
  render() {
  	const { isActivityFeedOpen, handleClose } = this.props;

    return (
				<Drawer
						open={isActivityFeedOpen}
						anchor="right"
						onClose={handleClose}
				>
					<Paper>
						<div className={theme.header}>
							<h3>Activity Feed</h3>
							<IconButton onClick={handleClose}>
								<CloseIcon />
							</IconButton>
						</div>
						<InboxNotifications />
					</Paper>
				</Drawer>
    );
  }
}

export default ActivityFeed;
