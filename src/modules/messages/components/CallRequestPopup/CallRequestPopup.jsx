import React from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import { get, isEmpty } from 'lodash';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import { MuiThemeProvider } from '@material-ui/core';
import CallIcon from '@material-ui/icons/Call';
import CallEnd from '@material-ui/icons/CallEnd';
import Typography from '@material-ui/core/Typography';
import colors from 'colors';
import { useStyles } from './CallRequestPopup-styles';

const theme = createMuiTheme({
  palette: {
    primary: { main: colors.accents.accent_green },
    secondary: { main: colors.accents.accent_bright_red, }
  }
});

export default function CallRequestPopup(props) {
  const { videoCallAccept, videoCallDecline, request } = props;
  const classes = useStyles();
  const open = !isEmpty(request);
  const last_name = get(request, 'created_by.last_name');
  const first_name = get(request, 'created_by.first_name');

  return (
    <div className={classes.container}>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => videoCallDecline(request)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.childContainer}>
            <Typography variant="h6">{`User ${first_name} ${last_name}, request video call`}</Typography>

            <MuiThemeProvider theme={theme}>
              <div className={classes.buttonsGroup}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={() => videoCallAccept(request)}
                >
                  <CallIcon />
                </Button>
                <Button
                  variant="contained"
									color="secondary"
                  className={classes.button}
                  onClick={() => videoCallDecline(request)}
                >
									<CallEnd />
                </Button>
              </div>
            </MuiThemeProvider>

          </div>
        </Fade>
      </Modal>
    </div>
  );
}
