import { createStyles, makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles((theme) => createStyles({
  toolbarButton: {
    marginRight: 10,
    minHeight: '36px',
  },
  toolbarIcon: {
    fill: theme.palette.decline.main,
  },
  container: {
    backgroundColor: theme.palette.common.white
  },

  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  childContainer: {
    backgroundColor: theme.palette.common.white,
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: '2px',
    boxShadow: `4px 2px 2px ${theme.palette.primary.main}`,
    padding: '20px'
  },
  buttonsGroup: {
    display: 'flex'
  },
  button: {
    flex: 1,
  }

}));
