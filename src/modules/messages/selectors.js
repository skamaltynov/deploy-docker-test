import { get } from 'lodash';
import { createSelector } from 'reselect';

export const isCreateMessageOpenSelector = ({ messages: state }) => get(state, 'createMessageOpen', false);
export const recipientsListSelector = ({ messages: state }) => get(state, 'recipientsList', []);
export const recipientsOptionsSelector = createSelector(
  recipientsListSelector,
  (providers) => providers.map(({ first_name, last_name, id }) => ({ key: id, label: `${first_name} ${last_name}` })),
);
export const selectedRecipientSelector = ({ messages: state }) => get(state, 'selectedRecipient', '');
export const allNotificationSelector = ({ messages: state }) => get(state, 'inboxNotifications', []);

export const lastRequestSelector = ({ messages: state }) => get(state, 'directRequest', {});

export const inboxNotificationsSelector = createSelector(
  allNotificationSelector,
  (notifications) => {
    return notifications.filter((item) => {
      const type = get(item, 'payload.type', '');

      return type !== 'direct_call';
    });
  }
);

export const inboxNotificationsCountSelector = createSelector(
  inboxNotificationsSelector,
  (notifications) => notifications.length
);

export const tokenSelector = (state) => get(state, 'token.token.teledentApiToken', '');

export const isActivityFeedOpenSelector = ({ messages: state }) => get(state, 'isActivityFeedOpen', false);

export const isPrivateChatOpenSelector = ({ messages: state }) => get(state, 'isPrivateChatOpen', false);

export const privateMessagesSelector = ({ messages: state }) => get(state, 'messages', []);

export const currentUserSelector = (state) => get(state, 'token.token', {});

export const recipientIdSelector = ({ messages: state }) => get(state, 'recipientId', '');

export const directVideoCallSelector = createSelector(
  allNotificationSelector,
  (notifications) => {
    return notifications.find((item) => {
      const type = get(item, 'payload.type', '');

      return type === 'direct_call';
    });
  }
);
