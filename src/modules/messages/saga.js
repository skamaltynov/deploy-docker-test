import { call, put, select, take, takeEvery } from 'redux-saga/effects';
import { race } from '@redux-saga/core/effects';
import * as at from './actions/actionTypes';
import * as a from './actions';
import { privateMessagesSelector } from './selectors';
import APImethods from '../../api';
import history from '../../history';

function* fetchRecipientsListSaga() {
  try {
    const { data: recipientsList } = yield call([APImethods, 'get'], 'teledentAccounts');

    yield put(a.setRecipientsList(recipientsList));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* makeDirectVideoCallSaga({ payload: id }) {
  const notificationData = {
    recipient_id: id,
    payload: {
      type: 'direct_call',
    },
    body: 'direct_call'
  };

  if (id) {
    try {
      const { data: roomId } = yield call([APImethods, 'get'], `videoconference/getroomid/${id}`);

      yield call([APImethods, 'post'], 'Notifications/send', notificationData);
      yield put(a.setProviderToProvider(true));
      yield history.push(`/conference/${roomId}/${id}`);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e);
    }
  }
}

function* handleIncomingRequests({ payload: request }) {
  try {
    const { data: roomId } = yield call([APImethods, 'get'], `videoconference/getroomid/${request.created_by.id}`);

    const [accept, decline] = yield race([
      take(at.DIRECT_VIDEO_CALL_ACCEPT),
      take(at.DIRECT_VIDEO_CALL_DECLINE),
    ]);

    if (decline) {
      yield put(a.deleteNotification(request.id));
    }
    if (accept) {
      yield history.push(`/conference/${roomId}/${request.recipient_id}`);
      yield put(a.deleteNotification(request.id));
    }
    yield put(a.setProviderToProvider(true));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}
function* openPrivateChatSaga({ payload: id }) {
  yield put(a.setRecipientId(id));
  yield put(a.togglePrivateChat(true));
}

function* fetchPersonalChatSaga({ payload: id }) {
  try {
	  const { data: messages } = yield call([APImethods, 'get'], `notes/getchatwith/${id}`);

	  yield put(a.setPersonalChatData(messages));
  } catch (e) {
    // eslint-disable-next-line no-console
	  console.log(e);
  }
}

function* deleteNotificationSaga({ payload }) {
  try {
    yield call([APImethods, 'post'], `notifications/acknowledge/${payload}`);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* sendMessageSaga({ payload: { message, id } }) {
  const messageData = {
	  body: message,
	  account_id: id,
	  title: 'title'
  };

  const messages = yield select(privateMessagesSelector);

  try {
    const { data } = yield call([APImethods, 'post'], 'Notes', messageData);

	  yield put(a.setPersonalChatData([...messages, data]));
    yield put(a.pushMessageNotification(message, id));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* pushMessageNotificationSaga({ payload: { message, id } }) {
  const notificationData = {
  	recipient_id: id,
	  payload: {
  		type: 'private_message',
		  message
	  },
	  body: message
  };

  try {
    yield call([APImethods, 'post'], 'Notifications/send', notificationData);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* pushVideoConferenceNotificationSaga({ payload }) {
  const { provider_id: recipient_id } = payload;
  const notificationData = {
    recipient_id,
    payload: {
    	conferenceData: payload
    },
    body: 'Video Conferencing',
	  type: 'Video Conferencing'
  };

  try {
    yield call([APImethods, 'post'], 'Notifications/send', notificationData);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

export default function* accountSaga() {
  yield takeEvery(at.SEND_PERSONAL_MESSAGE, sendMessageSaga);
  yield takeEvery(at.PUSH_MESSAGE_NOTIFICATION, pushMessageNotificationSaga);
  yield takeEvery(at.DELETE_NOTIFICATION, deleteNotificationSaga);
  yield takeEvery(at.PUSH_VIDEO_CONFERENCING_NOTIFICATION, pushVideoConferenceNotificationSaga);
  yield takeEvery(at.FETCH_PERSONAL_CHAT_DATA, fetchPersonalChatSaga);
  yield takeEvery(at.OPEN_PRIVATE_CHAT, openPrivateChatSaga);
  yield takeEvery(at.MAKE_DIRECT_VIDEO_CALL, makeDirectVideoCallSaga);
  yield takeEvery(at.FETCH_RECIPIENTS_LIST, fetchRecipientsListSaga);
  yield takeEvery(at.DIRECT_VIDEO_CALL_INCOMING_REQUEST, handleIncomingRequests);
}
