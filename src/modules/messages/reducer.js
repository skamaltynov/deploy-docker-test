import * as at from './actions/actionTypes';

const initialState = {
  createMessageOpen: false,
  inboxNotifications: [],
  isActivityFeedOpen: false,
  isPrivateChatOpen: false,
  messages: [],
  recipientId: '',
  recipientsList: [],
  directRequest: {}
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case at.TOGGLE_CREATE_MESSAGE:
      return {
        ...state,
        createMessageOpen: payload,
      };

	  case at.SET_RECIPIENT:
		  return {
			  ...state,
			  selectedRecipient: payload,
		  };

	  case at.SET_INBOX_NOTIFICATION:
		  return {
			  ...state,
			  inboxNotifications: payload
		  };

	  case at.TOGGLE_ACTIVITY_FEED:
		  return {
			  ...state,
			  isActivityFeedOpen: payload
		  };

	  case at.TOGGLE_PRIVATE_CHAT:
		  return {
			  ...state,
			  isPrivateChatOpen: payload
		  };

	  case at.SET_PERSONAL_CHAT_DATA:
		  return {
			  ...state,
			  messages: payload
		  };

    case at.SET_RECIPIENT_ID:
		  return {
			  ...state,
			  recipientId: payload
		  };
  	case at.DIRECT_VIDEO_CALL_INCOMING_REQUEST:
		  return {
			  ...state,
			  directRequest: payload
		  };

	  case at.SET_RECIPIENTS_LIST:
		  return {
			  ...state,
			  recipientsList: payload
		  };
	  case at.DIRECT_VIDEO_CALL_ACCEPT:
	  	   return {
			   ...state,
			   directRequest: {}
		   };
	  case at.DIRECT_VIDEO_CALL_DECLINE:
		  return {
			  ...state,
			  directRequest: {}
		  };
    default:
      return state;
  }
};
