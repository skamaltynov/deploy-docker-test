import { LOGIN_ERROR, LOGIN_REQUESTING, LOGIN_SUCCESS, SET_USER_INFO } from './actions/actionTypes';

const initialState = {
  requesting: false,
  successful: false,
  messages: [],
  errors: [],
};

const reducer = function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUESTING:
      return {
        requesting: true,
        successful: false,
        messages: [{ body: 'Logging in...', time: new Date() }],
        errors: [],
      };

    case LOGIN_SUCCESS:
      return {
        errors: [],
        messages: [],
        requesting: false,
        successful: true,
      };

    case LOGIN_ERROR:
      return {
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      };
    case SET_USER_INFO:
      return {
        userInfo: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
