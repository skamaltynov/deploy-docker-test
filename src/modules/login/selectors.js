import { get } from 'lodash';

export const currentUserSelector = ({ login: state }) => get(state, 'teledentUserInfo');
export const authTokenSelector = ({ token: state }) => `Bearer ${get(state, 'token.token')}`;
