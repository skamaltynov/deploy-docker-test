import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { SET_AUTHORIZATION_HEADER, } from './actions/actionTypes';
import { getUserInfo, setUserInfo } from '../conference/actions';
import APImethods, { instance } from '../../api';
import { GET_USER_INFO } from '../conference/actions/actionTypes';
import { enqueueSnackbar } from '../notifier/actions';

export function handleApiErrors(response) {
  if (!response.ok) throw Error(response.statusText);
  return response;
}

function* getUserInfoSaga() {
  try {
    const { data: { teledentUserInfo } } = yield call([APImethods, 'get'], 'account/info');

    yield put(setUserInfo(teledentUserInfo));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    yield call(setAuthorizationHeader, { payload: null });
    yield put(enqueueSnackbar('Authorization token not valid', {
      variant: 'error',
    }));
  }
}

function* setAuthorizationHeader({ payload: authToken }) {
  if (authToken) {
    yield instance.defaults.headers.common.Authorization = `Bearer ${authToken}`;
    yield put(getUserInfo());
  } else {
    delete instance.defaults.headers.common.Authorization;
  }
}

export function* loginWatcher() {
  yield takeLatest(SET_AUTHORIZATION_HEADER, setAuthorizationHeader);
  yield takeEvery(GET_USER_INFO, getUserInfoSaga);
}
