import React from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => createStyles({
  root: {
    fontWeight: 'bold',
    listStyle: 'none',
    padding: 0,
    margin: [[theme.spacing.unit, 0]],
  },

}));
const Messages = (props) => {
  const { messages } = props;
  const classes = useStyles();

  return (
    <div>
      <ul className={classes.root}>
        {messages.map((message) => (
          <li key={message.time}>{message.body}</li>
        ))}
      </ul>
    </div>
  );
};

export default Messages;
