import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import connect from 'react-redux/es/connect/connect';
import { loginRequest } from '../../actions';
import Errors from '../Errors';
import Messages from '../Messages';

const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    hintText={label}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
);

class Login extends Component {
  submit = (values) => {
    this.props.loginRequest(values);
  };

  render() {
    const {
      handleSubmit,
      login: {
        requesting,
        messages,
        errors,
      },
    } = this.props;

    return (
      <Container maxWidth="xs">
        <Box display="flex" alignItems="center" flexDirection="column" mt={4}>
          <Avatar>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">Sign in</Typography>
          <form onSubmit={handleSubmit(this.submit)}>
            <Field
              component={renderTextField}
              variant="outlined"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <Field
              component={renderTextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              disabled={requesting}
              fullWidth
              variant="contained"
              color="primary"
              mt={2}
            >
              Sign In
            </Button>
            <div className="auth-messages">
              {/* As in the signup, we're just using the message and error helpers */}
              {!requesting && !!errors.length && (
                <Errors message="Failure to login due to:" errors={errors} />
              )}
              {!requesting && !!messages.length && (
                <Messages messages={messages} />
              )}
              {/* {requesting && <div>Logging in...</div>} */}
              {/* {!requesting && !successful && ( */}
              {/*  < Link to="/signup">Need to Signup? Click Here »</Link> */}
              {/* )} */}
            </div>
            {/* <Grid container> */}
            {/*  <Grid item xs> */}
            {/*    <Link href="#" variant="body2">Forgot password?</Link> */}
            {/*  </Grid> */}
            {/* </Grid> */}
          </form>
        </Box>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  login: state.login,
});

const connected = connect(mapStateToProps, { loginRequest })(Login);
const formed = reduxForm({
  form: 'login',
})(connected);

export default formed;
