import React, { Component } from 'react';
import { Redirect, Route, withRouter } from 'react-router-dom';
import * as PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';
import { checkAuthorization } from '../actions';

class PrivateRoute extends Component {
  state = {
    authTokens: false,
  };

  static getDerivedStateFromProps(props) {
    return { authTokens: checkAuthorization(props.dispatch) };
  }

  render() {
    const {
      // eslint-disable-next-line no-shadow
      login, component: Component, dispatch, ...rest
    } = this.props;

    return (
      <Route
        {...rest}
        render={(props) => (this.state.authTokens ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { referer: props.location } }}
          />
        ))}
      />
    );
  }
}

PrivateRoute.propTypes = { component: PropTypes.any };
const mapStateToProps = (state) => ({
  login: state.login,
});

const connected = withRouter(connect(mapStateToProps)(PrivateRoute));

export default connected;
