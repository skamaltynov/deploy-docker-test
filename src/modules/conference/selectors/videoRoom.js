import _, { differenceBy, get, isEqual, some, values } from 'lodash';
import { createSelector, createSelectorCreator, defaultMemoize } from 'reselect';

export const pipStateSelector = ({ conference: state }) => get(state, 'pip');
export const appointmentSelector = ({ conference: state }) => get(state, 'janusConfig.appointment');
export const janusConfigSelector = ({ conference: state }) => get(state, 'janusConfig');
export const notificationSelector = ({ conference: state }) => get(state, 'notification', {});
export const janusRoomSelector = ({ conference: state }) => get(state, 'janusConfig.room');
export const isRecordSelector = ({ conference: state }) => get(state, 'janusConfig.room.record');
export const feedsSelector = (state) => {
  return values(get(state, 'conference.feeds', {}));
};
export const currentJanusUserSelector = ({ conference: state }) => get(state, 'currentUser', {
  id: null,
  name: 'NoName'
});
// const errorSelector = state => state.conference.mcu.error || state.conference.error || null
// const janusInstanceSelector = state => state.conference.mcu.janus
// const addedFeedSelector = state => state.conference.addedFeed

const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  isEqual
);

export const videoDevicesSelector = ({ conference: state }) => get(state, 'videoDevices');

export const remoteFeedsSelector = createDeepEqualSelector(
  feedsSelector,
  (feeds) => feeds
    .filter((feed) => (feed.remote
      && feed.feedName !== 'recording'
    ))
  ,
);

export const localFeedsSelector = createDeepEqualSelector(
  feedsSelector,
  (feeds) => feeds
    .filter((feed) => (!feed.remote
      && feed.feedName !== 'recording'
    ))
  ,
);
export const maxY = 12;
export const maxX = 24;
export const margin = 10;

export const layoutSelector = createSelector(
  localFeedsSelector,
  remoteFeedsSelector,
  (localFeeds, remoteFeeds) => {
    const layout = [];
    const leftOffset = 3;

    const addItem = (feed, x, y, w, h) => {
      const sufffix = `${remoteFeeds.length}${localFeeds.length}`;

      layout.push({
        i: `${feed.id}${sufffix}`,
        feed,
        x,
        y,
        w,
        h
      });
    };

    if (remoteFeeds.length && localFeeds.length) {
      _.each(localFeeds, (feed, i) => {
        const colCount = localFeeds.length > 2 ? localFeeds.length : 3;
        const h = Math.ceil((maxY / colCount));

        addItem(feed, 0, h * i, leftOffset, h);
      });
      _.each(remoteFeeds, (feed, index) => {
        const colCount = Math.ceil(Math.sqrt(remoteFeeds.length));

        const h = Math.ceil((maxY / colCount));
        const w = Math.floor((maxX - leftOffset) / colCount);

        const rowPosition = Math.floor(index / colCount);
        const colPosition = index % colCount;

        addItem(feed, colPosition * w + leftOffset, rowPosition * h, w, h);
      });
    } else if (localFeeds.length) {
      if (localFeeds.length === 1) {
        const feed = localFeeds[0];

        addItem(feed, 0, 0, maxX, maxY);
      } else {
        _.each(localFeeds, (feed, i) => {
          const colCount = localFeeds.length > 2 ? localFeeds.length : 3;
          const h = Math.ceil((maxY / colCount));

          addItem(feed, 0, h * i, leftOffset, h);
        });
      }
    }
    return layout;
  },
);


export const usedDevicesSelector = createDeepEqualSelector(
  localFeedsSelector,
  (feeds) => feeds
    .filter((feed) => (get(feed, 'stream')
          && get(feed, 'stream').getVideoTracks()
          && get(feed, 'stream').getVideoTracks().length))
    .map((feed) => {
      return ({ label: feed.stream.getVideoTracks()[0].label });
    })
);

export const notUsedDevicesSelector = createDeepEqualSelector(
  videoDevicesSelector,
  usedDevicesSelector,
  (videoDevices, UsedDevices) => differenceBy(videoDevices, UsedDevices, 'label'),
);

export const isScreenShareSelector = createDeepEqualSelector(
  localFeedsSelector,
  (feeds) => some(feeds, { feedName: 'screenshare' }),
);

export const isProviderToProviderSelector = ({ conference: state }) => get(state, 'providerToProvider');

export const isAllowRemoveFeedSelector = createDeepEqualSelector(
  localFeedsSelector,
  (feeds) => feeds.length > 1
);
