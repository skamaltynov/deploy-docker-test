import _ from 'lodash';
import { call, cancelled, delay, fork, put, race, select, take, takeEvery, } from '@redux-saga/core/effects';
import { eventChannel } from '@redux-saga/core';
import userMedia from '../../utils/userMedia';
import Janus from '../../utils/janus-es';
import * as a from './actions';
import * as t from './actions/actionTypes';
import * as janusActions from './actions/janusAsyncActions';
import { enqueueSnackbar } from '../notifier/actions';
import { defaultVideoDeviceSelector } from '../imageCapture/selectors';
import APImethods from '../../api';
import { currentJanusUserSelector, feedsSelector, janusConfigSelector, janusRoomSelector } from './selectors/videoRoom';
import { SET_USER_INFO } from '../login/actions/actionTypes';

const recording = 'recording';

let instanceJanus;


let mypvtid = null;

const timers = {};
const opaqueId = `videoroom-${Janus.randomString(12)}`;

const getRoomServerSettings = (state) => state.config.roomConfig;

export const makeDisplay = (user, feedName) => `${user.id}___${feedName}___${user.name}`;
export const getUserFromDisplay = (display) => {
  const [id, feedName, name] = (display).split('___');

  return { id, feedName, name, display };
};

function createLocalFeedEventChannel(feedName, offer, user) {
  return eventChannel((emitter) => {
    const addRemoteFeed = (publishers) => {
      Janus.debug('Got a list of available publishers/feeds:');
      Janus.debug(publishers);
      _.each(publishers, (p) => {
        emitter(a.roomRemoteFeedAtach(p.id, JSON.parse(p.display)));
      });
    };

    user = {
      ...user,
      display: makeDisplay(user, feedName)
    };

    const feed = {
      feedName,
      user,
      status: 'connecting',
      remote: false,
      offer,
    };

    instanceJanus.attach({
      plugin: 'janus.plugin.videoroom',
      opaqueId,
      success: (pluginHandle) => {
        feed.plugin = pluginHandle;
        Janus.log(`Plugin attached! (${feed.plugin.getPlugin()}, id=${feed.plugin.getId()})`);
        Janus.log('  -- This is a publisher/manager');
        emitter(a.joinToRoom(user, feed.plugin));
      },
      error: (error) => {
        Janus.error('  -- Error attaching plugin...', error);
        // feeds = _.map(feeds, (f) => {
        //   if (f.id === feed.id) {
        //     feed.status = 'error';
        //   }
        //   return f;
        // });
        // emitter(a.roomLocalFeed(feed, feeds.slice(0)));
        emitter(a.attachMcuError(error));
      },
      consentDialog: (on) => {
        Janus.debug(`Consent dialog should be ${on ? 'on' : 'off'} now`);
      },
      mediaState: (medium, on) => {
        Janus.log(`Janus ${on ? 'started' : 'stopped'} receiving our ${medium}`);
      },
      webrtcState: (on) => {
        Janus.log(`Janus says our WebRTC PeerConnection is ${on ? 'up' : 'down'} now`);
      },
      ondataopen: () => {
        Janus.debug(' ::: Data open (publisher) :::');
        emitter(a.roomDataOpen());
      },

      ondata: (data) => {
        Janus.debug(' ::: On Data (publisher) :::');
        emitter(a.roomRemoteData(data));
      },
      onmessage: (msg, jsep) => {
        Janus.debug(` ::: Got a message (publisher) ::: \n ${JSON.stringify(msg)}`);
        const event = msg.videoroom;

        Janus.debug(` ::: Event: ${event}`);
        if (event) {
          switch (event) {
            case 'joined': {
              const { publishers, id, private_id } = msg;

              mypvtid = private_id;
              feed.id = id;
              feed.status = 'paused';
              Janus.log(`Successfully joined room ${msg.room} with ID ${mypvtid}`);
              if (publishers) {
                addRemoteFeed(publishers);
              }
              emitter(a.roomLocalFeed(feed));
              break;
            }
            case 'destroyed': // The room has been destroyed
              Janus.warn('The room has been destroyed!');
              emitter(a.roomDestroyed());
              break;
            case 'event': // Any new feed to attach to?
              const removeId = msg.leaving || msg.unpublished;

              if (msg.publishers) {
                addRemoteFeed(msg.publishers);
              } else if (removeId) {
                Janus.log(`Publisher left: ${removeId}`);
                emitter(a.roomPublisherLeft(removeId));
              } else if (msg.error) {
                // Some other error
                // feeds = _.map(feeds, (f) => {
                //   if (f.id === feed.id) {
                //     feed.status = 'error';
                //   }
                //   return f;
                // });
                // emitter(a.roomLocalFeed(feed, feeds.slice(0)));
                Janus.error('  -- Feed Event Error', msg.error);
              }
              break;
            case 'simulcast':
              break;
            default:
              break;
          }
        }
        if (jsep) {
          Janus.debug('Handling SDP as well...');
          Janus.debug(jsep);
          feed.plugin.handleRemoteJsep({ jsep });
        }
      },
      onlocalstream: (stream) => {
        // Local video / audio

        Janus.debug(' ::: Got a local stream :::');
        if (feed.feedName === recording) {
          stream.getVideoTracks()[0].onended = () => emitter(a.configureRoom({
            record: false,
          }));
        }
        // if (feed.feedName === 'screenshare') {
        //
        // }
        emitter(a.roomLocalStream(feed, stream, 'connected'));
      },
      onremotestream: () => {
        // The publisher stream is sendonly, we don't expect anything here
      },
      oncleanup: () => {
        // Unpublish
        Janus.log(' ::: Got a cleanup notification: we are unpublished now :::');
        emitter(a.roomCleanUp());
      },
      destroyed() {
        Janus.log(' ::: FEED destroyed');
        // window.location.reload();
      }
    });
    // The subscriber must return an unsubscribe function
    return () => {
      // emitter(END)

      // clearInterval(iv)
    };
  });
  // Attach to video MCU test plugin
}

function createRemoteFeedEventChannel(id, display, room, currentUserId) {
  return eventChannel((emitter) => {
    let bitrateTimer;
    const user = getUserFromDisplay(display);
    const { feedName } = user;

    const feed = {
      feedName,
      id,
      user,
      status: 'connecting',
      remote: true,
    };

    instanceJanus.attach({
      plugin: 'janus.plugin.videoroom',
      opaqueId,
      success: (pluginHandle) => {
        feed.plugin = pluginHandle;
        // We wait for the plugin to send us an offer
        Janus.log(`Plugin attached! (${feed.plugin.getPlugin()}, id=${feed.plugin.getId()})`);
        Janus.log('  -- This is a subscriber');

        const listen = {
          request: 'join', room, ptype: 'subscriber', feed: feed.id, private_id: mypvtid,
        };

        feed.plugin.send({ message: listen });
      },
      error: (error) => {
        Janus.error('  -- Error attaching plugin...', error);
        emitter(a.attachMcuError(error));
      },
      onmessage: (msg, jsep) => {
        const event = msg.videoroom;

        Janus.debug(` ::: Got a message (subscriber) ::: \n ${JSON.stringify(msg)}`);
        if (event) {
          if (event === 'attached') {
            Janus.log(`Successfully attached to feed ${feed.plugin.rfid} 
              (${feed.plugin.rfdisplay}) in room ${msg.room}`);
            emitter(a.roomRemoteFeed(feed));
          }
          if (event === 'simulcast') {
            // Is simulcast in place?
            // var substream = result["substream"];
            // var temporal = result["temporal"];
            // if ((substream !== null && substream !== undefined) || (temporal !== null && temporal !== undefined)) {
            //   if (!simulcastStarted) {
            //     simulcastStarted = true;
            //     addSimulcastButtons(result["videocodec"] === "vp8");
            //   }
            //   // We just received notice that there's been a switch, update the buttons
            //   updateSimulcastButtons(substream, temporal);
            // }
          }
        }
        if (jsep) {
          // Answer and attach
          Janus.debug('Handling SDP as well...');
          Janus.debug(jsep);
          feed.plugin.createAnswer({
            jsep,
            media: {
              audioSend: false,
              videoSend: false,
              data: true,
            },
            // eslint-disable-next-line no-shadow
            success: (jsep) => {
              const body = { request: 'start', room };

              Janus.debug('Got SDP!');
              Janus.debug(jsep);
              feed.plugin.send({ message: body, jsep });
            },
            error: (error) => {
              Janus.error('WebRTC error:', error);
              emitter(a.roomRemoteFeedError(error));
            },
          });
        }
      },
      webrtcState: (on) => {
        Janus.log(`Janus says this WebRTC PeerConnection (feed #${feed.user.id}) is ${on ? 'up' : 'down'} now`);
      },
      ondataopen: () => {
        emitter(a.roomDataOpen());
      },

      ondata: (data) => {
        // const {id} = getState().conference.janusConfig.display
        const receivedData = JSON.parse(data);

        if (currentUserId !== receivedData.user.id) {
          switch (receivedData.type) {
            case 'chat':
              emitter(a.roomRemoteChatData(receivedData));
              break;
            case 'roomConfigBroadcast':
              emitter(a.configureRoom(receivedData));
              break;
            default:
              break;
          }
        }
      },
      onlocalstream: () => {

      },
      onremotestream: (stream) => {
        Janus.debug(`Remote feed #${feed.id}`);
        const { pc } = feed.plugin.webrtcStuff;

        if (Janus.webRTCAdapter.browserDetails.browser === 'chrome'
            || Janus.webRTCAdapter.browserDetails.browser === 'firefox'
            || Janus.webRTCAdapter.browserDetails.browser === 'safari') {
          timers[feed.id] = setInterval(() => {
            if (!feed.plugin.detached) {
              const bitrate = feed.plugin.getBitrate();

              if (bitrate !== 'Invalid handle') {
                emitter(a.roomRemoteUpdateStats(feed.id, { bitrate }));
              } else {
                clearInterval(timers[feed.id]);
                timers[feed.id] = 0;
              }
            }
          }, 1000);
        }

        if (pc) {
          pc.ontrack = (event) => {
            const changeStatus = (ev) => {
              let status = pc.iceConnectionState;

              if (ev.currentTarget.muted) {
                status = 'muted';
              }
              emitter(a.roomRemoteStream(feed, stream, status));
            };

            event.track.onmute = changeStatus;
            event.track.onunmute = changeStatus;
          };
          pc.oniceconnectionstatechange = () => {
            emitter(a.roomRemoteStream(feed, stream, pc.iceConnectionState));
          };
        }
        emitter(a.roomRemoteStream(feed, stream, feed.status));
      },
      oncleanup: () => {
        if (timers[feed.id]) {
          clearInterval(timers[feed.id]);
          timers[feed.id] = 0;
        }
        Janus.log(` ::: Got a cleanup notification (remote feed ${feed.plugin.id}) :::`);
      },
    });
    return () => {
      clearInterval(bitrateTimer);
    };
  });
}

function* setDevicesSaga() {
  while (true) {
    const videoDevices = yield userMedia.getVideoDevices();
    const audioDevices = yield userMedia.getAudioDevices();

    yield put(a.setVideoDevices(videoDevices));
    yield put(a.setAudioDevices(audioDevices));

    yield delay(t.MEDIA_PING_DELAY);
  }
  // yield takeLatest(at.SELECT_LOCAL_DEVICES, selectLocalDevices);
  // yield takeLatest(at.SELECT_MOUTH_WATCH_DEVICES, selectMouthWatchDevices)
}

function* joinToRoomSagaAction(plugin, room, user) {
  while (true) {
    try {
      yield call(janusActions.isRoomExistsAsync, plugin, room);
      yield call(janusActions.joinToRoomAsync, plugin, room, user);
      break;
    } catch (e) {
      if (room.request && room.request === 'create') {
        yield call(janusActions.createRoomAsync, plugin, room);
        yield call(janusActions.joinToRoomAsync, plugin, room, user);
      }
    }
  }
}

function* createJanusSessionSagaAction(config) {
  try {
    instanceJanus = yield call(janusActions.createJanusSessionAsync, config);

    yield put(a.createSessionSuccess(instanceJanus, config));
    yield delay(2000);
  } catch (e) {
    yield put(a.createSessionError(e));
  } finally {
    if (yield cancelled()) {
      yield call(janusActions.destroyJanusSessionAsync, instanceJanus);
    }
  }
}
async function getMedia(constraints) {
  let stream = null;

  try {
    stream = await navigator.mediaDevices.getUserMedia(constraints);
    /* use the stream */
  } catch (err) {
    /* handle the error */
  }
  return stream;
}
const stream = getMedia();

function* subscribeCreateJanusSessionSaga() {
  while (true) {
    const config = yield select(janusConfigSelector);
    const { roomId } = yield take(t.CREATE_SESSION);

    const stream = yield call(getMedia, { audio: true,
      video: {
        width: { min: 480, ideal: 1280, max: 1920 },
        height: { min: 320, ideal: 720, max: 1080 }
      } });

    const feeds = {
      0: {
        feedName: 'HD-камера ',
        user: {
          id: 0,
          name: 'localUserName',
          GUID: '12426625-f3e7-4772-8e6d-e8b224da0458',
          display: '0___HD-камера ___localUserName'
        },
        status: 'connected',
        remote: false,
        offer: {
          media: {
            video: {
              deviceId: 'bd5c900186541e3d4545939527a6e983fc14ff6c051d1db5a91ac241e7763e5c'
            },
            audio: true,
            data: true
          }
        },
        plugin: {
          session: {
            destroyOnUnload: true
          },
          plugin: 'janus.plugin.videoroom',
          id: 0,
          token: null,
          detached: false,
          webrtcStuff: {
            started: false,
            myStream: {},
            streamExternal: false,
            remoteStream: null,
            mySdp: 'v=0\r\no=- 6163656494793578004 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0 1 2\r\na=msid-semantic: WMS oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 110 112 113 126\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:xJTR\r\na=ice-pwd:DLGHK6eqCEbXzGrURf/Zt3uS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 D4:8B:56:B4:CE:2F:E5:B4:3B:9B:EF:3E:81:4C:9C:20:96:E2:B3:BF:8D:F1:CE:0E:6C:31:CD:41:30:BA:02:3A\r\na=setup:actpass\r\na=mid:0\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:2 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:6 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=sendrecv\r\na=msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 4c88dce3-71f5-4979-b2ad-09634f72d50a\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=rtcp-fb:111 transport-cc\r\na=fmtp:111 minptime=10;useinbandfec=1\r\na=rtpmap:103 ISAC/16000\r\na=rtpmap:104 ISAC/32000\r\na=rtpmap:9 G722/8000\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:106 CN/32000\r\na=rtpmap:105 CN/16000\r\na=rtpmap:13 CN/8000\r\na=rtpmap:110 telephone-event/48000\r\na=rtpmap:112 telephone-event/32000\r\na=rtpmap:113 telephone-event/16000\r\na=rtpmap:126 telephone-event/8000\r\na=ssrc:1498536148 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:1498536148 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 4c88dce3-71f5-4979-b2ad-09634f72d50a\r\na=ssrc:1498536148 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:1498536148 label:4c88dce3-71f5-4979-b2ad-09634f72d50a\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97 98 99 100 101 102 122 127 121 125 107 108 109 124 120 123 119 114 115 116\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:xJTR\r\na=ice-pwd:DLGHK6eqCEbXzGrURf/Zt3uS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 D4:8B:56:B4:CE:2F:E5:B4:3B:9B:EF:3E:81:4C:9C:20:96:E2:B3:BF:8D:F1:CE:0E:6C:31:CD:41:30:BA:02:3A\r\na=setup:actpass\r\na=mid:1\r\na=extmap:14 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:2 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=extmap:11 http://www.webrtc.org/experiments/rtp-hdrext/video-content-type\r\na=extmap:7 http://www.webrtc.org/experiments/rtp-hdrext/video-timing\r\na=extmap:8 http://tools.ietf.org/html/draft-ietf-avtext-framemarking-07\r\na=extmap:9 http://www.webrtc.org/experiments/rtp-hdrext/color-space\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:6 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=sendrecv\r\na=msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 transport-cc\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=rtpmap:98 VP9/90000\r\na=rtcp-fb:98 goog-remb\r\na=rtcp-fb:98 transport-cc\r\na=rtcp-fb:98 ccm fir\r\na=rtcp-fb:98 nack\r\na=rtcp-fb:98 nack pli\r\na=fmtp:98 profile-id=0\r\na=rtpmap:99 rtx/90000\r\na=fmtp:99 apt=98\r\na=rtpmap:100 VP9/90000\r\na=rtcp-fb:100 goog-remb\r\na=rtcp-fb:100 transport-cc\r\na=rtcp-fb:100 ccm fir\r\na=rtcp-fb:100 nack\r\na=rtcp-fb:100 nack pli\r\na=fmtp:100 profile-id=2\r\na=rtpmap:101 rtx/90000\r\na=fmtp:101 apt=100\r\na=rtpmap:102 H264/90000\r\na=rtcp-fb:102 goog-remb\r\na=rtcp-fb:102 transport-cc\r\na=rtcp-fb:102 ccm fir\r\na=rtcp-fb:102 nack\r\na=rtcp-fb:102 nack pli\r\na=fmtp:102 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42001f\r\na=rtpmap:122 rtx/90000\r\na=fmtp:122 apt=102\r\na=rtpmap:127 H264/90000\r\na=rtcp-fb:127 goog-remb\r\na=rtcp-fb:127 transport-cc\r\na=rtcp-fb:127 ccm fir\r\na=rtcp-fb:127 nack\r\na=rtcp-fb:127 nack pli\r\na=fmtp:127 level-asymmetry-allowed=1;packetization-mode=0;profile-level-id=42001f\r\na=rtpmap:121 rtx/90000\r\na=fmtp:121 apt=127\r\na=rtpmap:125 H264/90000\r\na=rtcp-fb:125 goog-remb\r\na=rtcp-fb:125 transport-cc\r\na=rtcp-fb:125 ccm fir\r\na=rtcp-fb:125 nack\r\na=rtcp-fb:125 nack pli\r\na=fmtp:125 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f\r\na=rtpmap:107 rtx/90000\r\na=fmtp:107 apt=125\r\na=rtpmap:108 H264/90000\r\na=rtcp-fb:108 goog-remb\r\na=rtcp-fb:108 transport-cc\r\na=rtcp-fb:108 ccm fir\r\na=rtcp-fb:108 nack\r\na=rtcp-fb:108 nack pli\r\na=fmtp:108 level-asymmetry-allowed=1;packetization-mode=0;profile-level-id=42e01f\r\na=rtpmap:109 rtx/90000\r\na=fmtp:109 apt=108\r\na=rtpmap:124 H264/90000\r\na=rtcp-fb:124 goog-remb\r\na=rtcp-fb:124 transport-cc\r\na=rtcp-fb:124 ccm fir\r\na=rtcp-fb:124 nack\r\na=rtcp-fb:124 nack pli\r\na=fmtp:124 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=4d0032\r\na=rtpmap:120 rtx/90000\r\na=fmtp:120 apt=124\r\na=rtpmap:123 H264/90000\r\na=rtcp-fb:123 goog-remb\r\na=rtcp-fb:123 transport-cc\r\na=rtcp-fb:123 ccm fir\r\na=rtcp-fb:123 nack\r\na=rtcp-fb:123 nack pli\r\na=fmtp:123 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=640032\r\na=rtpmap:119 rtx/90000\r\na=fmtp:119 apt=123\r\na=rtpmap:114 red/90000\r\na=rtpmap:115 rtx/90000\r\na=fmtp:115 apt=114\r\na=rtpmap:116 ulpfec/90000\r\na=ssrc:4233552759 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:4233552759 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:4233552759 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:4233552759 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:2253563249 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:2253563249 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:2253563249 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:2253563249 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:556684357 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:556684357 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:556684357 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:556684357 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:1132219407 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:1132219407 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:1132219407 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:1132219407 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:1707070677 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:1707070677 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:1707070677 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:1707070677 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:4278042543 cname:lacpxDH1Cb8OJ7Ir\r\na=ssrc:4278042543 msid:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT 13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc:4278042543 mslabel:oWijJsMYX3iANUALfu04wgZgW1lWl2osepeT\r\na=ssrc:4278042543 label:13f25b66-ccbc-4ddc-8223-d323ec582864\r\na=ssrc-group:SIM 4233552759 556684357 1707070677\r\na=ssrc-group:FID 4233552759 2253563249\r\na=ssrc-group:FID 556684357 1132219407\r\na=ssrc-group:FID 1707070677 4278042543\r\nm=application 9 UDP/DTLS/SCTP webrtc-datachannel\r\nc=IN IP4 0.0.0.0\r\na=ice-ufrag:xJTR\r\na=ice-pwd:DLGHK6eqCEbXzGrURf/Zt3uS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 D4:8B:56:B4:CE:2F:E5:B4:3B:9B:EF:3E:81:4C:9C:20:96:E2:B3:BF:8D:F1:CE:0E:6C:31:CD:41:30:BA:02:3A\r\na=setup:actpass\r\na=mid:2\r\na=sctp-port:5000\r\na=max-message-size:262144\r\n',
            mediaConstraints: {},
            pc: {},
            dataChannel: {
              JanusDataChannel: {
                pending: []
              }
            },
            dtmfSender: null,
            trickle: true,
            iceDone: true,
            volume: {},
            bitrate: {
              value: '0 kbits/sec',
              bsnow: null,
              bsbefore: null,
              tsnow: null,
              tsbefore: null,
              timer: null
            },
            candidates: [],
            remoteSdp: 'v=0\r\no=- 6163656494793578004 2 IN IP4 100.24.120.77\r\ns=VideoRoom 1006\r\nt=0 0\r\na=group:BUNDLE 0 1 2\r\na=msid-semantic: WMS janus\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 100.24.120.77\r\na=recvonly\r\na=mid:0\r\na=rtcp-mux\r\na=ice-ufrag:WSaU\r\na=ice-pwd:mDxhfzgeDa65e7nEkcQFWS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:active\r\na=rtpmap:111 opus/48000/2\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:6 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=msid:janus janusa0\r\na=ssrc:1580876631 cname:janus\r\na=ssrc:1580876631 msid:janus janusa0\r\na=ssrc:1580876631 mslabel:janus\r\na=ssrc:1580876631 label:janusa0\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 100.24.120.77\r\na=recvonly\r\na=mid:1\r\na=rtcp-mux\r\na=ice-ufrag:WSaU\r\na=ice-pwd:mDxhfzgeDa65e7nEkcQFWS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:active\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 transport-cc\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=extmap:8 http://tools.ietf.org/html/draft-ietf-avtext-framemarking-07\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:6 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=msid:janus janusv0\r\na=ssrc:3119730858 cname:janus\r\na=ssrc:3119730858 msid:janus janusv0\r\na=ssrc:3119730858 mslabel:janus\r\na=ssrc:3119730858 label:janusv0\r\na=ssrc:3231524425 cname:janus\r\na=ssrc:3231524425 msid:janus janusv0\r\na=ssrc:3231524425 mslabel:janus\r\na=ssrc:3231524425 label:janusv0\r\nm=application 9 UDP/DTLS/SCTP webrtc-datachannel\r\nc=IN IP4 100.24.120.77\r\na=sendrecv\r\na=sctp-port:5000\r\na=mid:2\r\na=ice-ufrag:WSaU\r\na=ice-pwd:mDxhfzgeDa65e7nEkcQFWS\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:active\r\n'
          }
        },
        id: 0,
        stream,
      },
      1: {
        feedName: 'HD-камера ',
        id: 1,
        user: {
          id: '1',
          feedName: 'HD-камера ',
          name: 'test name',
          display: '1___HD-камера ___remote_user'
        },
        status: 'connected',
        remote: true,
        plugin: {
          session: {
            destroyOnUnload: true
          },
          plugin: 'janus.plugin.videoroom',
          id: 1,
          token: null,
          detached: false,
          webrtcStuff: {
            started: false,
            streamExternal: false,
            remoteStream: {},
            mySdp: 'v=0\r\no=- 1814447943414306180 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:audio\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=fmtp:111 minptime=10;useinbandfec=1\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:video\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 0.0.0.0\r\nb=AS:30\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:data\r\na=sctpmap:5000 webrtc-datachannel 1024\r\n',
            mediaConstraints: {},
            pc: {},
            dataChannel: {
              JanusDataChannel: {
                pending: []
              }
            },
            dtmfSender: null,
            trickle: true,
            iceDone: true,
            volume: {},
            bitrate: {
              value: '104 kbits/sec',
              bsnow: 6172849,
              bsbefore: 6172849,
              tsnow: 1581065563125.707,
              tsbefore: 1581065563125.707,
              timer: 110
            },
            candidates: [],
            remoteSdp: 'v=0\r\no=- 1581065095828980 1 IN IP4 100.24.120.77\r\ns=VideoRoom 1006\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS janus\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:audio\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:111 opus/48000/2\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=msid:janus janusa0\r\na=ssrc:2879970281 cname:janus\r\na=ssrc:2879970281 msid:janus janusa0\r\na=ssrc:2879970281 mslabel:janus\r\na=ssrc:2879970281 label:janusa0\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:video\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtcp-fb:96 goog-remb\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=ssrc-group:FID 1724132925 1004445007\r\na=msid:janus janusv0\r\na=ssrc:1724132925 cname:janus\r\na=ssrc:1724132925 msid:janus janusv0\r\na=ssrc:1724132925 mslabel:janus\r\na=ssrc:1724132925 label:janusv0\r\na=ssrc:1004445007 cname:janus\r\na=ssrc:1004445007 msid:janus janusv0\r\na=ssrc:1004445007 mslabel:janus\r\na=ssrc:1004445007 label:janusv0\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 100.24.120.77\r\na=sendrecv\r\na=sctpmap:5000 webrtc-datachannel 16\r\na=mid:data\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\n'
          }
        },
        stream,
        stats: {
          bitrate: '88 kbits/sec'
        }
      },
      2: {
        feedName: 'HD-камера ',
        id: 2,
        user: {
          id: '2',
          feedName: 'HD-камера ',
          name: 'test name',
          display: '2___HD-камера ___remote_user_2'
        },
        status: 'connected',
        remote: true,
        plugin: {
          session: {
            destroyOnUnload: true
          },
          plugin: 'janus.plugin.videoroom',
          id: 2,
          token: null,
          detached: false,
          webrtcStuff: {
            started: false,
            streamExternal: false,
            remoteStream: {},
            mySdp: 'v=0\r\no=- 1814447943414306180 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:audio\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=fmtp:111 minptime=10;useinbandfec=1\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:video\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 0.0.0.0\r\nb=AS:30\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:data\r\na=sctpmap:5000 webrtc-datachannel 1024\r\n',
            mediaConstraints: {},
            pc: {},
            dataChannel: {
              JanusDataChannel: {
                pending: []
              }
            },
            dtmfSender: null,
            trickle: true,
            iceDone: true,
            volume: {},
            bitrate: {
              value: '104 kbits/sec',
              bsnow: 6172849,
              bsbefore: 6172849,
              tsnow: 1581065563125.707,
              tsbefore: 1581065563125.707,
              timer: 110
            },
            candidates: [],
            remoteSdp: 'v=0\r\no=- 1581065095828980 1 IN IP4 100.24.120.77\r\ns=VideoRoom 1006\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS janus\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:audio\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:111 opus/48000/2\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=msid:janus janusa0\r\na=ssrc:2879970281 cname:janus\r\na=ssrc:2879970281 msid:janus janusa0\r\na=ssrc:2879970281 mslabel:janus\r\na=ssrc:2879970281 label:janusa0\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:video\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtcp-fb:96 goog-remb\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=ssrc-group:FID 1724132925 1004445007\r\na=msid:janus janusv0\r\na=ssrc:1724132925 cname:janus\r\na=ssrc:1724132925 msid:janus janusv0\r\na=ssrc:1724132925 mslabel:janus\r\na=ssrc:1724132925 label:janusv0\r\na=ssrc:1004445007 cname:janus\r\na=ssrc:1004445007 msid:janus janusv0\r\na=ssrc:1004445007 mslabel:janus\r\na=ssrc:1004445007 label:janusv0\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 100.24.120.77\r\na=sendrecv\r\na=sctpmap:5000 webrtc-datachannel 16\r\na=mid:data\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\n'
          }
        },
        stream,
        stats: {
          bitrate: '88 kbits/sec'
        }
      },
      3: {
        feedName: 'HD-камера ',
        id: 3,
        user: {
          id: '3',
          feedName: 'HD-камера ',
          name: 'test name',
          display: '3___HD-камера ___remote_user_3'
        },
        status: 'connected',
        remote: true,
        plugin: {
          session: {
            destroyOnUnload: true
          },
          plugin: 'janus.plugin.videoroom',
          id: 1,
          token: null,
          detached: false,
          webrtcStuff: {
            started: false,
            streamExternal: false,
            remoteStream: {},
            mySdp: 'v=0\r\no=- 1814447943414306180 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:audio\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=fmtp:111 minptime=10;useinbandfec=1\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:video\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 0.0.0.0\r\nb=AS:30\r\na=ice-ufrag:Dy2R\r\na=ice-pwd:vPnrxxeF+MCUik/dKUMPApsY\r\na=ice-options:trickle\r\na=fingerprint:sha-256 4F:E3:C3:94:24:20:53:D8:85:60:CE:CC:3B:D4:52:A7:74:E7:E1:A6:24:89:55:4A:FF:AB:0B:85:9D:1F:C1:26\r\na=setup:active\r\na=mid:data\r\na=sctpmap:5000 webrtc-datachannel 1024\r\n',
            mediaConstraints: {},
            pc: {},
            dataChannel: {
              JanusDataChannel: {
                pending: []
              }
            },
            dtmfSender: null,
            trickle: true,
            iceDone: true,
            volume: {},
            bitrate: {
              value: '104 kbits/sec',
              bsnow: 6172849,
              bsbefore: 6172849,
              tsnow: 1581065563125.707,
              tsbefore: 1581065563125.707,
              timer: 110
            },
            candidates: [],
            remoteSdp: 'v=0\r\no=- 1581065095828980 1 IN IP4 100.24.120.77\r\ns=VideoRoom 1006\r\nt=0 0\r\na=group:BUNDLE audio video data\r\na=msid-semantic: WMS janus\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:audio\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:111 opus/48000/2\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=msid:janus janusa0\r\na=ssrc:2879970281 cname:janus\r\na=ssrc:2879970281 msid:janus janusa0\r\na=ssrc:2879970281 mslabel:janus\r\na=ssrc:2879970281 label:janusa0\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 100.24.120.77\r\na=sendonly\r\na=mid:video\r\na=rtcp-mux\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtcp-fb:96 goog-remb\r\na=extmap:13 urn:3gpp:video-orientation\r\na=extmap:12 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=ssrc-group:FID 1724132925 1004445007\r\na=msid:janus janusv0\r\na=ssrc:1724132925 cname:janus\r\na=ssrc:1724132925 msid:janus janusv0\r\na=ssrc:1724132925 mslabel:janus\r\na=ssrc:1724132925 label:janusv0\r\na=ssrc:1004445007 cname:janus\r\na=ssrc:1004445007 msid:janus janusv0\r\na=ssrc:1004445007 mslabel:janus\r\na=ssrc:1004445007 label:janusv0\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 100.24.120.77\r\na=sendrecv\r\na=sctpmap:5000 webrtc-datachannel 16\r\na=mid:data\r\na=ice-ufrag:6cmQ\r\na=ice-pwd:4QQvgHFo7osmV3Zpeb91a+\r\na=ice-options:trickle\r\na=fingerprint:sha-256 AF:C2:86:9A:BE:7A:01:FA:8F:E9:C2:2F:C1:F7:AC:51:3C:51:B6:A5:AF:FF:42:A1:26:7D:10:B3:7B:3B:16:69\r\na=setup:actpass\r\n'
          }
        },
        stream,
        stats: {
          bitrate: '88 kbits/sec'
        }
      }
    };

    yield put(a.roomLocalFeed(feeds[0]));
    yield put(a.roomLocalFeed(feeds[1]));
    yield put(a.roomLocalFeed(feeds[2]));
    yield put(a.roomLocalFeed(feeds[3]));
    // yield put({ type: t.CREATE_SESSION, config: { roomId: room } });
    // debugger;
    //
    // yield take(SET_USER_INFO);
    //
    // const { data: room } = yield call([APImethods, 'get'], `videoconference/getroomid/${roomId}`);
    //
    // yield put({ type: t.CREATE_SESSION, config: { roomId: room } });
    //
    // yield call(createJanusSessionSagaAction, { ...config, room });
    // const { error } = yield race({
    //   error: take([t.CREATE_SESSION_ERROR]),
    //   stop: take(t.DESTROY_SESSION),
    // });
    //
    // yield call(janusActions.destroyJanusSessionAsync, instanceJanus);
    // if (error) {
    //   yield delay(1000);
    //   yield put({ type: t.CREATE_SESSION, config: { roomId: room } });
    // } else {
    //   yield put({ type: t.CLEAN_FEEDS });
    // }
  }
}
function* putFromChannel(value) {
  yield put(value);
}
function* subscribeAttachLocalChannelSaga(feedName, offer, user) {
  const channel = yield call(createLocalFeedEventChannel, feedName, offer, user);

  yield takeEvery(channel, putFromChannel);


  yield take(t.DESTROY_SESSION);
  channel.close();
}

function* subscribeAttachLocalFeedSaga() {
  while (true) {
    const { feedName, offer } = yield take(t.ROOM_LOCAL_FEED_ATTACH);
    const user = yield select(currentJanusUserSelector);

    yield fork(subscribeAttachLocalChannelSaga, feedName, offer, user);
  }
}

function* subscribeAttachRemoteChannelSaga(id, display, room, user) {
  const channel = yield call(createRemoteFeedEventChannel, id, display, room, user.id);

  yield takeEvery(channel, putFromChannel);
  yield take(t.DESTROY_SESSION);
  channel.close();
}


function* subscribeToRemoveRemoteFeed() {
  while (true) {
    const { payload: { id } } = yield take(t.ROOM_PUBLISHERS_LEFT);
    const feeds = yield select(feedsSelector);

    const removeFeed = _.find(feeds, { id });

    if (removeFeed) {
      let message = `User ${removeFeed.user.name}(${removeFeed.feedName}) left the room `;

      removeFeed.plugin.detach();
      if (timers[id]) {
        clearInterval(timers[id]);
        timers[id] = 0;
      }

      yield put(a.roomRemoveFeed(id));
      if (removeFeed.feedName === recording) {
        message = `User ${removeFeed.user.name} finished recording `;
      }
      if (removeFeed.remote) {
        yield put(enqueueSnackbar(message));
      }
    }
  }
}

function* subscribeAttachRemoteFeedSaga() {
  while (true) {
    const { id, display } = yield take(t.ROOM_REMOTE_FEED_ATTACH);
    const { room } = yield select(janusRoomSelector);
    const user = yield select(currentJanusUserSelector);

    const feeds = yield select(feedsSelector);
    const existedFeed = _.find(feeds, { id });

    if (!existedFeed) {
      const { feedName, name } = getUserFromDisplay(display);

      let message = `User ${name} joined to the room with ${feedName}`;

      if (feedName === recording) {
        message = `User ${name} started recording`;
      }
      yield put(enqueueSnackbar(message));

      yield fork(subscribeAttachRemoteChannelSaga, id, display, room, user);
    }
  }
}

function* subscribeJoinToRoomSaga() {
  while (true) {
    const { user, plugin } = yield take(t.JOIN_TO_ROOM);

    const { room } = yield select(janusConfigSelector);
    const roomConfig = yield select(getRoomServerSettings);

    roomConfig.rec_dir = `${roomConfig.videoRoomRecordPath}/${room.room}`;
    yield call(joinToRoomSagaAction, plugin, { ...room, ...roomConfig }, user);
  }
}

function* subscribeSessionStartSaga() {
  while (true) {
    yield take(t.CREATE_SESSION_SUCCESS);
    let video = true;

    let data = false;

    let feedName = 'default';
    const { deviceId, label } = yield select(defaultVideoDeviceSelector);

    if (deviceId) {
      feedName = label.substring(0, 10);
      video = {
        deviceId
      };
    }
    data = true;
    yield put(a.attachLocalFeed(feedName, {
      media: {
        video,
        audio: true,
        data,
      }
    }));
  }
}

function* subscribeToPublishFeedsSaga() {
  while (true) {
    const { payload } = yield take(t.ROOM_LOCAL_FEED);

    const { feed } = payload;

    yield call(janusActions.publishLocalFeedAsync, feed);
    yield put({ type: t.ROOM_LOCAL_PUBLISHED, payload });
    // yield put(enqueueSnackbar('Stream published'));
  }
}

function* subscribeToReplaceFeedSaga() {
  while (true) {
    const { feedName, offer } = yield take(t.ROOM_LOCAL_FEED_REPLACE);
    const feeds = yield select(feedsSelector);

    const feed = _.find(feeds, { feedName });

    yield call(janusActions.detachLocalFeedAsync, feed);

    const removeFeed = _.find(feeds, { feedName });

    if (removeFeed) {
      Janus.debug(`Feed ${removeFeed.rfid} (${removeFeed.rfdisplay}) has left the room, detaching`);
      removeFeed.plugin.detach();
      yield put(a.roomRemoveFeed(removeFeed.id));
    }
    yield put(a.attachLocalFeed(feedName, offer));
  }
}
function* subscribeToDetachLocalFeedSaga() {
  while (true) {
    const { payload } = yield take(t.ROOM_LOCAL_FEED_DEATTACH);
    const feeds = yield select(feedsSelector);
    const feed = _.find(feeds, { id: payload.id });

    yield call(janusActions.detachLocalFeedAsync, feed);
    // const removeFeed = _.find(feeds, { feedName });
    //
    // if (removeFeed) {
    //   Janus.debug(`Feed ${removeFeed.rfid} (${removeFeed.rfdisplay}) has left the room, detaching`);
    //   removeFeed.plugin.detach();
    //   yield put(a.roomRemoveFeed(removeFeed.id));
    // }
  }
}

function* subscribeToFeedToolbarEvent() {
  while (true) {
    const { feedName, option } = yield take(t.FEED_OPTION_TOGGLE);
    const feeds = yield select(feedsSelector);

    const feed = _.find(feeds, { feedName });

    const optionValue = _.get(feed, option, false);

    feed[option] = !optionValue;
    // yield put(a.feedOptionToggleChange(feeds.slice(0)));
    // yield call(janusActions.detachLocalFeedAsync, feed)
    // let removeFeed = feeds.filter(feed => feed.feedName === feedName)
    // if (removeFeed[0]) {
    //   removeFeed = removeFeed[0]
    //   feeds.splice(feeds.indexOf(removeFeed), 1)
    //   yield put(a.roomRemoveFeed(removeFeed, feeds.slice(0)))
    // }
  }
}

function* getRecordingStreem(feeds) {
  const audioContext = new AudioContext();

  const audioStreamDest = audioContext.createMediaStreamDestination();

  _.each(feeds, (f) => {
    if (f.stream) {
      const audio = new Audio();

      audio.muted = true;
      audio.srcObject = f.stream;

      const sources = f.stream.getAudioTracks().map((track) => audioContext
        .createMediaStreamSource(new MediaStream([track])));

      sources.forEach((source) => {
        // Add it to the current sources being mixed
        source.connect(audioStreamDest);
      });
    }
  });
  const screenStream = yield navigator.mediaDevices.getDisplayMedia({ video: true });

  const stream = new MediaStream([...screenStream.getVideoTracks(), ...audioStreamDest.stream.getAudioTracks()]);

  return stream;
}

function* subscribeToConfigureRoom() {
  let stream = null;

  while (true) {
    const { payload: { config } } = yield take(t.ROOM_CONFIGURE);
    // const { userName, janusPeerId } = yield select(currentUserSelector);
    const roomServerConfig = yield select(getRoomServerSettings);
    const roomConfig = yield select(janusRoomSelector);
    const newConfig = { ...config, ...roomServerConfig, ...roomConfig, record: config.record };

    if (_.has(config, 'record')) {
      if (config.record === false) {
        const feeds = yield select(feedsSelector);
        const recordingFeed = _.find(feeds, { feedName: recording });

        if (recordingFeed) {
          yield janusActions.configureRoomAsync(recordingFeed.plugin, newConfig);
          yield delay(1000);
          stream.getTracks().forEach((track) => track.stop());
          yield janusActions.detachLocalFeedAsync(recordingFeed);
        }

        yield put(enqueueSnackbar('Video start uploading to cloud'));
        yield delay(1000);
        yield call([APImethods, 'post'], `/VideoConference/${newConfig.room}/save`);
        yield put(enqueueSnackbar('Video succesfully uploaded'));
      } else {
        const feeds = yield select(feedsSelector);
        const feed = _.find(feeds, { feedName: 'screenshare' });
        const roomConfig = yield select(getRoomServerSettings);

        roomConfig.rec_dir = `${roomConfig.videoRoomRecordPath}/${newConfig.room}`;
        if (feed) {
          yield call(janusActions.detachLocalFeedAsync, feed);
        }
        stream = yield getRecordingStreem(feeds);

        yield put(a.attachLocalFeed(recording, {
          stream,
          media: { audio: true, video: true }
        }));
        const { payload } = yield take(t.ROOM_LOCAL_PUBLISHED);

        // eslint-disable-next-line no-undef
        yield janusActions.configureRoomAsync(payload.feed.plugin, newConfig);
      }
    }
    if (_.has(config, 'bitrate') && config.feedId) {
      const { bitrate, feedId } = config;
      const feeds = yield select(feedsSelector);

      const feed = _.find(feeds, { id: feedId });


      yield janusActions.configureRoomAsync(feed.plugin, { bitrate });
    }
    if ((_.has(config, 'substream') || _.has(config, 'temporal')) && config.feedId) {
      const { temporal, substream, feedId } = config;
      const feeds = yield select(feedsSelector);

      const feed = _.find(feeds, { id: feedId });


      yield janusActions.configureRoomAsync(feed.plugin, { substream, temporal });
    }

    // const optionValue = _.get(feed, option, false);

    // feed[option] = !optionValue;
    // yield put(a.feedOptionToggleChange(feeds.slice(0)));
    // yield call(janusActions.detachLocalFeedAsync, feed)
    // let removeFeed = feeds.filter(feed => feed.feedName === feedName)
    // if (removeFeed[0]) {
    //   removeFeed = removeFeed[0]
    //   feeds.splice(feeds.indexOf(removeFeed), 1)
    //   yield put(a.roomRemoveFeed(removeFeed, feeds.slice(0)))
    // }
  }
}
export default function* conferenceSaga() {
  yield fork(setDevicesSaga);
  // yield fork(subscribeAttachLocalFeedSaga);
  // yield fork(subscribeAttachRemoteFeedSaga);
  yield fork(subscribeCreateJanusSessionSaga);
  // yield fork(subscribeJoinToRoomSaga);
  // yield fork(subscribeSessionStartSaga);
  // yield fork(subscribeToRemoveRemoteFeed);
  // yield fork(subscribeToPublishFeedsSaga);
  // yield fork(subscribeToReplaceFeedSaga);
  // yield fork(subscribeToDetachLocalFeedSaga);
  // yield fork(subscribeToFeedToolbarEvent);
  // yield fork(subscribeToConfigureRoom);
}
