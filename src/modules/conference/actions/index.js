import * as at from './actionTypes';

export const setVideoDevices = (devices) => ({
  type: at.SET_VIDEO_DEVICES,
  payload: devices,
});

export const setAudioDevices = (devices) => ({
  type: at.SET_AUDIO_DEVICES,
  payload: devices,
});


export const createSession = (config) => ({
  type: at.CREATE_SESSION,
  config,
});
export const destroySession = () => ({
  type: at.DESTROY_SESSION,
});
export const createSessionSuccess = (instance, config) => ({ type: at.CREATE_SESSION_SUCCESS,
  payload: { instance, config } });
export const createSessionError = (e) => ({ type: at.CREATE_SESSION_ERROR, payload: e });
export const attachLocalFeed = (feedName, offer) => ({
  type: at.ROOM_LOCAL_FEED_ATTACH,
  feedName,
  offer,
});
export const detachLocalFeed = (id) => ({
  type: at.ROOM_LOCAL_FEED_DEATTACH,
  payload: { id },
});
export const replaceLocalFeed = (feedName, offer) => ({
  type: at.ROOM_LOCAL_FEED_REPLACE,
  feedName,
  offer,
});

export const configureRoom = (config) => ({
  type: at.ROOM_CONFIGURE,
  payload: { config }
});

export const roomRemoteFeedAtach = (id, display) => ({
  type: at.ROOM_REMOTE_FEED_ATTACH,
  id,
  display,
});
export const joinToRoom = (user, plugin) => ({
  type: at.JOIN_TO_ROOM,
  user,
  plugin,
});
export const attachMcuError = (error) => ({
  type: at.ATTACH_MCU_ERROR,
  message: error,
});
export const roomDataOpen = () => ({
  type: at.ROOM_DATA_OPEN,
});
export const roomLocalFeed = (feed) => ({
  type: at.ROOM_LOCAL_FEED,
  payload: { feed }
});
export const roomDestroyed = () => ({
  type: at.ROOM_DESTROYED,
});
export const roomRemoveFeed = (id) => ({
  type: at.ROOM_REMOVE_FEED,
  payload: { id }
});

export const roomPublisherLeft = (id) => ({
  type: at.ROOM_PUBLISHERS_LEFT,
  payload: { id }
});

export const roomCleanUp = (feed, feeds) => ({
  type: at.ROOM_CLEAN_UP,
  feed,
  feeds,
});

export const roomLocalStream = (feed, stream, status) => ({
  type: at.ROOM_LOCAL_STREAM,
  payload: { feed, stream, status }
});
export const roomIceError = (pc) => ({
  type: at.ROOM_ICE_ERROR,
  message: pc.iceConnectionState,
});

export const roomRemoteFeed = (feed) => ({
  type: at.ROOM_REMOTE_FEED,
  payload: { feed }
});
export const roomRemoteFeedError = (error) => ({
  type: at.ROOM_REMOTE_FEED_ERROR,
  message: error,
});

export const roomRemoteData = (data) => ({
  type: at.ROOM_REMOTE_DATA,
  payload: JSON.parse(data),
});

export const roomRemoteChatData = (data) => ({
  type: at.ROOM_DATA_CHAT,
  payload: data,
});

export const roomRemoteStream = (feed, stream, status) => ({
  type: at.ROOM_REMOTE_STREAM,
  payload: { feed, stream, status }
});
export const roomRemoteUpdateStats = (feedId, stats) => ({
  type: at.ROOM_REMOTE_UPDATE_STATS,
  payload: { feedId, stats }
});
export const feedOptionToggle = (feedName, option) => ({
  type: at.FEED_OPTION_TOGGLE,
  option,
  feedName,
});
export const feedOptionToggleChange = (feeds) => ({
  type: at.FEED_OPTION_TOGGLE_CHANGE,
  feeds,
});

export const pipToggle = (pip) => ({
  type: at.TOGGLE_PIP,
  payload: { pip },
});

export const showNotification = (type, message) => ({
  type: at.SET_NOTIFICATION,
  payload: { type, message },
});

export const resetNotification = () => ({
  type: at.RESET_NOTIFICATION,
});

export const getUserInfo = () => ({ type: at.GET_USER_INFO });
export const setUserInfo = (data) => ({
  type: at.SET_USER_INFO,
  payload: data
});
