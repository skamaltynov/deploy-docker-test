import adapter from 'webrtc-adapter';
import Janus from '../../../utils/janus-es';

export const createJanusSessionAsync = (config) => {
  return new Promise((resolve, error) => {
    const setupDeps = () => Janus.useDefaultDependencies({
      adapter,
    });

    Janus.init({
      debug: !!config.debug,
      dependencies: setupDeps(),
      callback: () => {
        // eslint-disable-next-line
        // console.log('Janus init DONE');
      },
    });
    const instanseJanus = new Janus(
      {
        ...config.janus,
        success: () => {
          resolve(instanseJanus);
        },
        error,
      },
    );
  });
};

export const destroyJanusSessionAsync = (instance) => new Promise((success, error) => {
  instance.destroy({
    success,
    error
  });
});

export const isRoomExistsAsync = (plugin, room) => new Promise((resolve, error) => {
  const message = {
    request: 'exists',
    room: room.room,
  };

  plugin.send({
    message,
    success: (result) => {
      if (result.exists && result.exists !== 'false') {
        resolve(result);
      } else {
        error(result);
      }
    },
    error,
  });
});

export const joinToRoomAsync = (plugin, room, user) => new Promise((success, error) => {
  const message = {
    request: 'join',
    room: room.room,
    ptype: 'publisher',
    display: JSON.stringify(user.display),
  };

  plugin.send({
    message,
    success,
    error
  });
});

export const createRoomAsync = (plugin, room) => new Promise((success, error) => {
  const message = { ...room };

  plugin.send({
    message,
    success,
    error
  });
});
export const configureRoomAsync = (plugin, config) => new Promise((success, error) => {
  const message = { request: 'configure',
    ...config };

  plugin.send({
    message,
    success,
    error
  });
});

export const publishLocalFeedAsync = (feed) => {
  const { plugin, offer } = feed;

  let { simulcast = true } = offer;
  const { audio = true, video = true, data = true } = offer;

  let record = false;

  if (feed.feedName === 'recording') {
    record = true;
    simulcast = false;
  }

  const message = {
    request: 'configure',
    // bitrate: 1024000,
    record,
    data,
    simulcast,
    audio: !!audio,
    video: !!video,
  };

  return new Promise((success, error) => {
    const offerOptions = {
      ...offer,
      simulcast,
      media: {
        simulcast,
        ...offer.media,
        data,
      },
      success: (jsep) => {
        Janus.debug('Got publisher SDP!');
        Janus.debug(jsep);
        plugin.send({
          message,
          jsep,
          success,
          error,
        });
      },
      error,
    };

    Janus.debug('Creating offer', offerOptions);
    plugin.createOffer(offerOptions);
  });
};

export const sendDataChannelMessageAsync = (feed, message = {}, type = 'chat') => new Promise((success, error) => {
  const text = JSON.stringify({ type, ...message });

  feed.plugin.data({
    text,
    success,
    error
  });
});

export const detachLocalFeedAsync = (feed) => new Promise((success, error) => {
  const message = { request: 'unpublish' };

  feed.plugin.send({
    message,
    success,
    error,
  });
});
