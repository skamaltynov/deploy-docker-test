import { withStyles } from '@material-ui/core';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Conference from './Conference';
import { useStyles } from './Conference-styles';
import { setAuthorizationHeader } from '../../../login/actions';
import * as s from '../../selectors/videoRoom';

import {
  attachLocalFeed,
  configureRoom,
  createSession,
  destroySession,
  detachLocalFeed,
  feedOptionToggle,
  pipToggle,
  replaceLocalFeed
} from '../../actions';
import { addNewImage } from '../../../imageCapture/actions';
import { loadConfig } from '../../../serverConfig/actions';

const mapStateToProps = (state) => ({
  notUsedDevices: s.notUsedDevicesSelector(state),
  isScreenShare: s.isScreenShareSelector(state),
  currentUser: s.currentJanusUserSelector(state),
  appointment: s.appointmentSelector(state),
  isAllowRemoveFeed: s.isAllowRemoveFeedSelector(state),
  layout: s.layoutSelector(state),
  pip: s.pipStateSelector(state),
  isRecord: s.isRecordSelector(state),
  isProviderToProvider: s.isProviderToProviderSelector(state),
  notification: s.notificationSelector(state),
  config: state.config.roomConfig
});
const mapDispatchToProps = (dispatch) => ({
  createSession: (config) => dispatch(createSession(config)),
  destroySession: () => dispatch(destroySession()),
  attachLocalFeed: (feedName, offer) => dispatch(attachLocalFeed(feedName, offer)),
  detachLocalFeed: (feedName) => dispatch(detachLocalFeed(feedName)),
  configureRoom: (config) => dispatch(configureRoom(config)),
  addNewImage: (image) => dispatch(addNewImage(image)),
  pipToggle: (pip) => dispatch(pipToggle(pip)),
  loadConfig: () => dispatch(loadConfig()),
  setAuthorizationHeader: (token) => dispatch(setAuthorizationHeader(token)),
  feedOptionToggle: (feedName, option) => dispatch(feedOptionToggle(feedName, option)),
  replaceLocalFeed: (feedName, offer) => dispatch(replaceLocalFeed(feedName, offer)),
});

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps, mapDispatchToProps)
)(Conference);
