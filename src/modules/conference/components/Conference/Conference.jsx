import React, { PureComponent } from 'react';
import { map as lMap, isEqual } from 'lodash';
import RGL, { WidthProvider } from 'react-grid-layout';
import Box from '@material-ui/core/Box';
import { SizeMe } from 'react-sizeme';
import AppBar from '@material-ui/core/AppBar/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Chat from '../../../chat';
import { margin, maxX, maxY } from '../../selectors/videoRoom';
import Stream from '../Stream';
import RecordButton from '../RecordButton';
import AddScreenSharingButton from '../ScreenSharingButton';
import AddCameraButton from '../AddCameraButton';
import { ToggleChatButton } from '../ToggleChatButton/ToggleChatButton';
import ExitButton from '../ExitButton';
import Notification from '../../../../components/Notification';

const ReactGridLayout = WidthProvider(RGL);

class Conference extends PureComponent {
  constructor(props) {
    super(props);

    const { match: { params: { roomId } } } = props;
    const { searchParams } = new URL(window.location.href);
    const authToken = searchParams.get('authToken');

    props.createSession({ roomId });
    props.setAuthorizationHeader(authToken);
    props.loadConfig();

    this.state = {
      showNotification: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { notification } = this.props;

    if (!isEqual(notification, prevProps.notification)) this.setState({ showNotification: true });
  }

  componentWillUnmount() {
    this.props.destroySession();
  }

  handleClose = () => {
    this.setState({ showNotification: false });
  };

  toggleChatHandler = () => {
    this.setState((prevState) => ({ showChat: !prevState.showChat }));
  };

  handleChatClose = () => {
    this.setState({ showChat: false });
  };

  render() {
    const {
      showChat,
    } = this.state;
    const {
      notification: { type, message },
      classes, replaceLocalFeed, pipToggle,
      pip, configureRoom, isRecord,
      feedOptionToggle, attachLocalFeed, detachLocalFeed, isScreenShare, notUsedDevices, isAllowRemoveFeed,
      layout, isProviderToProvider
    } = this.props;
    const { showNotification } = this.state;

    return (
      <>
          <Box
              className={classes.root}
              display="flex"
              p={0}
              flexDirection="column"
          >
            {layout.length > 0 && (
                <SizeMe monitorHeight>
                  {({ size }) => {
                    return (
                        <ReactGridLayout
                            className={`${classes.main} layout`}
                            rowHeight={Math.ceil(((size.height - 10) + (margin * 4)) / maxY) - margin * 1.5}
                            maxRows={maxY}
                            margin={[margin, margin]}
                            autoSize={false}
                            verticalCompact
                            preventCollision={false}
                            cols={maxX}
                        >
                          {lMap(layout, (el) => (
                              <div key={el.i} data-grid={el}>
                                <Stream
                                    feed={el.feed}
                                    name={el.feed.feedName}
                                    stream={el.feed.stream}
                                    fullscreen={el.feed.fullscreen}
                                    pip={pip}
                                    pipToggle={pipToggle}
                                    isProviderToProvider={isProviderToProvider}
                                    canRemove={!el.feed.remote && isAllowRemoveFeed}
                                    configureRoom={configureRoom}
                                    replaceLocalFeed={replaceLocalFeed}
                                    detachLocalFeed={detachLocalFeed}
                                    feedOptionToggle={feedOptionToggle}
                                />
                              </div>
                          ))}
                        </ReactGridLayout>
                    );
                  }}
                </SizeMe>
            )}

            {layout.length === 0 && (<div className={`${classes.main} layout`} />)}

            <Box className={classes.footer} p={0} bgcolor="background.paper">
              <AppBar position="static">
                <Toolbar className={classes.toolbar}>
                  <div className={classes.toolbarWrapper}>
                    <RecordButton
                        configureRoom={configureRoom}
                        attachLocalFeed={attachLocalFeed}
                        detachLocalFeed={detachLocalFeed}
                        isRecord={isRecord}
                    />

                    {!isScreenShare && (
                        <AddScreenSharingButton
                            attachLocalFeed={attachLocalFeed}
                            detachLocalFeed={detachLocalFeed}
                        />
                    )}

                    {!!notUsedDevices.length && (
                        <AddCameraButton
                            classes={classes}
                            devices={notUsedDevices}
                            attachLocalFeed={attachLocalFeed}
                        />
                    )}
                    <ToggleChatButton classes={classes} onClick={this.toggleChatHandler} />
                  </div>
                  <ExitButton classes={classes} />
                </Toolbar>
              </AppBar>
            </Box>
            {showNotification && <Notification type={type} message={message} onClose={this.handleClose} />}
          </Box>

        {showChat && <Chat onChatClose={this.handleChatClose} />}
      </>
    );
  }
}

export default Conference;
