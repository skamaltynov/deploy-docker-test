const bottomHeight = 72;

export const useStyles = ({ palette: { common } }) => ({
  root: {
    height: '100%',
  },
  main: {
    flex: 1,
    width: '100%',
    overflow: 'hidden',
    position: 'relative',
    flexBasis: `calc(100% - ${bottomHeight * 3}px)`,
  },

  footer: {
    flex: 0,
    position: 'relative',
    zIndex: 1,

  },
  chatButton: {
    marginLeft: '10px',
    minHeight: '36px',
  },
  chatIcon: {
    marginLeft: '8px',
    fill: common.white,
  },
  toolbar: {
    justifyContent: 'space-between'
  },
  toolbarWrapper: {
    flex: 1,
    display: 'flex',
    flexWrap: 'nowrap'
  },


  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
