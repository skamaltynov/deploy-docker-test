import { connect } from 'react-redux';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core';
import Stream from './Stream';
import { useStyles } from './Stream-styles';
import { addNewImage } from '../../../imageCapture/actions';

const mapDispatchToProps = (dispatch) => ({
  addNewImage: (data) => dispatch(addNewImage(data))
});

export default compose(
  withStyles(useStyles),
  connect(null, mapDispatchToProps)
)(Stream);
