import PropTypes from 'prop-types';
import _ from 'lodash';
import React, { PureComponent } from 'react';
import ReactPlayer from 'react-player';
import { findDOMNode } from 'react-dom';
import Card from '@material-ui/core/Card';
import screenfull from 'screenfull';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import CardActions from '@material-ui/core/CardActions';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import MicOff from '@material-ui/icons/MicOff';
import Mic from '@material-ui/icons/Mic';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import PictureInPictureAltIcon from '@material-ui/icons/PictureInPictureAlt';
import Input from '@material-ui/core/Input';
import { getUserFromDisplay } from '../../saga';
import Loading from '../../../../components/Loading';

class Stream extends PureComponent {
  static propTypes = {
    addNewImage: PropTypes.func.isRequired,
    configureRoom: PropTypes.func.isRequired,
    appointmentId: PropTypes.string,
    canRemove: PropTypes.bool.isRequired,
    classes: PropTypes.any.isRequired,
    detachLocalFeed: PropTypes.func.isRequired,
    feed: PropTypes.object.isRequired,
    isProviderToProvider: PropTypes.bool.isRequired,
    minScreenshotHeight: PropTypes.number,
    minScreenshotWidth: PropTypes.number,
    pip: PropTypes.bool.isRequired,
    pipToggle: PropTypes.func.isRequired,
    stream: PropTypes.any
  };

  state = {
    isMute: false,
    isAudio: false,
    substream: '0',
    bitrate: '0'
  };

  constructor(props) {
    super(props);
    this.menuRef = React.createRef();
  }

  static getDerivedStateFromProps(props) {
    if (_.get(props, 'feed.stream') && _.get(props, 'feed.plugin') && !_.get(props, 'feed.remote')) {
      const { plugin, stream } = props.feed;
      // const isMute = plugin.isAudioMuted();
      const isMute = false;

      let isAudio = false;

      const audioTracks = stream.getAudioTracks();

      if (audioTracks.length) {
        isAudio = true;
      }
      return { isAudio, isMute };
    }
    return null;
  }

  ref = (player) => {
    if (player) {
      this.player = player;
      this.video = player.getInternalPlayer();
    }
  }

  onReady = (player) => {
    const { feed } = this.props;
    const video = player.getInternalPlayer();

    if (video) {
      this.video = video;
      if (!feed.remote) {
        video.muted = 'muted';
      }
    }
  }


  handleRemoveClick = () => {
    this.props.detachLocalFeed(this.props.feed.id);
  };

  handleScreenShot = () => {
    const { ctx, props, video } = this;

    if (!video) {
      return null;
    }
    if (!ctx) {
      const canvas = document.createElement('canvas');
      const aspectRatio = video.videoWidth / video.videoHeight;

      let canvasWidth = props.minScreenshotWidth || video.clientWidth;

      let canvasHeight = canvasWidth / aspectRatio;

      if (props.minScreenshotHeight
          && canvasHeight < props.minScreenshotHeight) {
        canvasHeight = props.minScreenshotHeight;
        canvasWidth = canvasHeight * aspectRatio;
      }
      // canvas.width = canvasWidth;
      // canvas.height = canvasHeight;
      canvas.width = canvasWidth;
      canvas.height = canvasHeight;
      this.canvas = canvas;
      this.ctx = canvas.getContext('2d');
    }
    this.ctx.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);
    const dataUrl = this.canvas.toDataURL();

    this.ctx.canvas.toBlob((blob) => {
      this.props.addNewImage({
        appointmentId: props.appointmentId,
        title: `image-${new Date().getTime()}`,
        dataUrl,
        status: 'loading',
        blob
      });
    });

    return this.canvas;
  };


  handleFullScreen = () => {
    screenfull.request(findDOMNode(this.player));
  };

  handleMute = () => {
    const { feed: { plugin } } = this.props;

    let isMute = plugin.isAudioMuted();

    if (isMute) plugin.unmuteAudio();
    else plugin.muteAudio();
    isMute = plugin.isAudioMuted();
    this.setState({ isMute });
  }

  handleChangeBitrate = (event) => {
    const bitrate = parseInt(event.target.value, 8) * 1000;

    this.props.configureRoom({ bitrate, feedId: this.props.feed.id });
    this.setState({ bitrate: event.target.value });
  }

  handleChangeSubstream = (event) => {
    const substream = parseInt(event.target.value, 8);

    this.props.configureRoom({ substream, temporal: substream, feedId: this.props.feed.id });
    this.setState({ substream: event.target.value });
  }

  handlePipClick = async () => {
    if (document.pictureInPictureEnabled) {
      try {
        await this.video.requestPictureInPicture();
        this.props.pipToggle(true);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    } else {
      await document.exitPictureInPicture();
    }
  };

  render() {
    const { stream, classes, feed, pipToggle, pip, canRemove, isProviderToProvider } = this.props;
    const { isMute, isAudio, bitrate, substream } = this.state;
    const display = _.get(feed, 'user.display');

    if (!display) { return null; }

    const { feedName, name } = getUserFromDisplay(display);

    return (
        <div className={classes.root}>
          <Card className={classes.card}>
            <CardHeader
                className={classes.header}

                titleTypographyProps={{ variant: 'h6', noWrap: true, }}
                title={`${name} (${feedName}) ${feed.status}`}
            />
            <CardContent className={classes.content}>

              {/* {(feed.status === 'connected' || !feed.remote) && ( */}
              <ReactPlayer
                  ref={this.ref}
                  className={`streamWrapper  ${classes.video}`}
                  onReady={this.onReady}
                  onDisablePIP={() => { pipToggle(false); }}
                  url={(feed.status === 'connected' || !feed.remote) ? stream : null}
                  controls={false}
                  // muted
                  autoPlay
                  playing
              />
              {/* )} */}
              {!(feed.status === 'connected' || !feed.remote) && <Loading />}

            </CardContent>
            <CardActions disableSpacing className={classes.footer}>
              <span style={{ color: '#fff' }}>{_.get(this, 'props.feed.stats.bitrate', '')}</span>
              {(!feed.remote && isAudio) && (
                  <IconButton aria-label="MicOff" size="small" color="secondary" onClick={this.handleMute}>
                    {isMute && (
                        <MicOff />
                    )}
                    {!isMute && (
                        <Mic />
                    )}
                    {/* <Mic /> */}
                  </IconButton>
              )}

              <IconButton
                  aria-label="Fullscreen"
                  onClick={this.handleFullScreen}
                  size="small"
                  color="secondary"
              >
                <FullscreenIcon />
              </IconButton>
              {(!feed.remote && !isProviderToProvider) && (
                  <IconButton
                              onClick={this.handleScreenShot}
                              aria-label="Make photo from camera"
                              size="small"
                              color="secondary"
                  >
                    <PhotoCameraIcon />
                  </IconButton>
              )}
              {(!pip) && (
                  <IconButton
                      size="small"
                      onClick={this.handlePipClick}
                      aria-label="Picture in picture"
                      color="secondary"
                  >
                    <PictureInPictureAltIcon />
                  </IconButton>
              )}
              {(canRemove) && (
                  <IconButton
                      size="small"
                      onClick={this.handleRemoveClick}
                      aria-label="Remove"
                      color="secondary"
                  >
                    <HighlightOffIcon />
                  </IconButton>
              )}

              {!feed.remote && (
<Select
                  className={classes.bitrate}
                  value={bitrate}
                  name="bitrate"
                  classes={{ icon: classes.icon }}
                  input={(
                      <Input
                          classes={{
                            focused: classes.focused,
                            disabled: classes.disabled,
                            error: classes.error,
                            underline: classes.underline
                          }}
                      />
                  )}
                  onChange={this.handleChangeBitrate}
>
                <MenuItem value={0}>∞</MenuItem>
                <MenuItem value={128}>128kbit</MenuItem>
                <MenuItem value={256}>256kbit</MenuItem>
                <MenuItem value={512}>512kbit</MenuItem>
                <MenuItem value={1025}>1mbit</MenuItem>
                <MenuItem value={1500}>1.5mbit</MenuItem>
                <MenuItem value={2000}>2mbit</MenuItem>
</Select>
              )}
              {feed.remote && (
                  <Select
                      className={classes.resolution}
                      value={substream}
                      name="substream"
                      classes={{ icon: classes.icon }}
                      input={(
                          <Input
                              classes={{
                                focused: classes.focused,
                                disabled: classes.disabled,
                                error: classes.error,
                                underline: classes.underline
                              }}
                          />
                      )}
                      onChange={this.handleChangeSubstream}
                  >
                    <MenuItem value="0">Lower quality</MenuItem>
                    <MenuItem value="1">Normal quality</MenuItem>
                    <MenuItem value="2">Higher quality</MenuItem>
                  </Select>
              )}


            </CardActions>
          </Card>


        </div>
    );
  }
}


export default Stream;
