import PropTypes from 'prop-types';
import React from 'react';
import Popper from '@material-ui/core/Popper';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';

export function StreamMenu(props: {}) {
  return (
    <Popper
      open={props.open}
      anchorEl={props.menuRef}
      placement="left-start"
      transition
      disablePortal
    >
      {({ TransitionProps }) => (
        <Grow
          {...TransitionProps}
        >
          <Paper id="menu-list-grow">
            <ClickAwayListener onClickAway={props.onClickAway}>
              <MenuList>
                {props.canScreenShot && (
                  <MenuItem
                    key="screenshot"
                    onClick={props.onClickScreenShot}
                  >
                    Take screenshot
                  </MenuItem>
                )}
                {props.canMute && (
                    <MenuItem
                        key="mute"
                        onClick={props.onClickMute}
                    >
                      {props.isMute ? 'Unmute me' : 'Mute me'}
                    </MenuItem>
                )}
                <MenuItem
                  key="fullscreen"
                  onClick={props.onClickFullScreen}
                >
                  Fullscreen
                </MenuItem>
                {props.canRemove && (
                  <MenuItem
                    key="remove"
                    onClick={props.onClickRemove}
                  >
                    Remove
                  </MenuItem>
                )}
                {!props.pip && (
                  <MenuItem
                    key="pip"
                    onClick={props.onClickPip}
                  >
                    Pip
                  </MenuItem>
                )}
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  );
}

StreamMenu.propTypes = {
  canRemove: PropTypes.bool,
  canScreenShot: PropTypes.bool,
  menuRef: PropTypes.any,
  onClickAway: PropTypes.func,
  onClickFullScreen: PropTypes.func,
  onClickPip: PropTypes.func,
  onClickRemove: PropTypes.func,
  onClickScreenShot: PropTypes.func,
  open: PropTypes.bool,
  pip: PropTypes.bool
};
