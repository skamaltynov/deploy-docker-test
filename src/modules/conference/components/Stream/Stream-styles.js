
export const useStyles = ({ palette: { primary, common }, spacing, shape }) => ({
  root: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },

  card: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    boxShadow: `0px 2px 30px -5px ${primary.disabled_gray}`
  },
  header: {
    flex: 0,
    backgroundColor: primary.darkBlue,
    borderTopLeftRadius: shape.borderRadius,
    borderTopRightRadius: shape.borderRadius,
    boxSizing: 'border-box',
    color: primary.contrastText,
    padding: spacing(1, 3),
    width: '100%',
    '& > .MuiCardHeader-content': {
      width: '50%',
    },
  },
  focused: {},
  disabled: {},
  error: {},
  icon: {
    fill: common.white,
  },
  underline: {
    '&:before': {
      // normal
      borderBottom: `0px solid ${common.white}`
    },
    '&:after': {
      // focused
      borderBottom: `0px solid ${common.white}`
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      // hover
      borderBottom: `0px solid ${common.white}`
    }
  },
  action: {
    marginBottom: '-8px'
  },
  bitrate: {
    position: 'absolute',
    top: '-35px',
    right: '10px',
    zIndex: 10,
    color: common.white,
    '&:before': {
      // normal
      borderBottom: '0px solid #fff'
    },
    '&:after': {
      // focused
      borderBottom: `0px solid ${common.white}`
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      // hover
      borderBottom: `0px solid ${common.white}`
    }
  },
  resolution: {
    // position: 'absolute',
    // top: 0,
    // right: 0,
    // zIndex: 10,
    color: common.white,
    '&:before': {
      // normal
      borderBottom: `0px solid ${common.white}`
    },
    '&:after': {
      // focused
      borderBottom: `0px solid ${common.white}`
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      // hover
      borderBottom: `0px solid ${common.white}`
    }
  },
  content: {
    padding: 0,
    width: '100%',
    flex: '1',

    background: common.black,
    position: 'relative',
    height: '1%'
  },
  footer: {
    padding: 0,
    position: 'relative',
    background: common.black,
    justifyContent: 'center'
  },
  video: {
    maxWidth: '100%',
    display: 'flex!important',
    width: '100%!important',
    height: '100%!important',
    '& video': {
      width: '100%!important',
      height: 'auto!important',
      maxWidth: '100%',
      maxHeight: '100%',
    },
  },


});
