import React from 'react';
import Button from '@material-ui/core/Button';
import ExitToApp from '@material-ui/icons/ExitToApp';

import { useStyles } from './ExitButton-styles';

const ExitButton = () => {
  const classes = useStyles();
  const handleClick = () => {
    window.close();
  };

  return (
    <Button
      color="primary"
      variant="contained"
      size="small"
      className={classes.toolbarButton}
      aria-haspopup="true"
      onClick={handleClick}
    >

      <ExitToApp className={classes.toolbarIcon} />
    </Button>
  );
};

export default ExitButton;
