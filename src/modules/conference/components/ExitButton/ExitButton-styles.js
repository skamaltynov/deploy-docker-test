import { createStyles, makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles((theme) => createStyles({
  toolbarButton: {
    float: 'right',
    marginRight: 10,
    minHeight: '36px',
  },
  toolbarIcon: {
    fill: theme.palette.common.white,
  },
}));
