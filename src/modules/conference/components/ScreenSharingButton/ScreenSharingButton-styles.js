import { createStyles, makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles((theme) => createStyles({
  toolbarButton: {
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    minHeight: '36px',
  },
  toolbarIcon: {
    fill: theme.palette.common.white,
  }
}));
