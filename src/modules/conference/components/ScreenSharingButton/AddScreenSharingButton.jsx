import PropTypes from 'prop-types';
import React from 'react';
import Button from '@material-ui/core/Button';
import { ScreenShareOutlined } from '@material-ui/icons';
import Hidden from '@material-ui/core/Hidden';
import { useStyles } from './ScreenSharingButton-styles';

const AddScreenSharingButton = ({ attachLocalFeed }) => {
  const [isScreenShare, setScreenShare] = React.useState(false);
  const classes = useStyles();
  const toggle = () => {
    if (!isScreenShare) {
      attachLocalFeed('screenshare', {
        media: {
          video: 'screen',
          audioRecv: false,
          videoRecv: false,
          audioSend: false,
          videoSend: true,
          screenshareFrameRate: 1,
        }
      });
      setScreenShare((prevIsScreenShare) => !prevIsScreenShare);
    }
  };

  return (
    <Button
      onClick={toggle}
      color="primary"
      variant="contained"
      className={classes.toolbarButton}
    >
      <Hidden mdDown>
      screenshare
      </Hidden>
      <ScreenShareOutlined className={classes.toolbarIcon} />
    </Button>
  );
};

export default AddScreenSharingButton;

AddScreenSharingButton.propTypes = {
  attachLocalFeed: PropTypes.func,
};

AddScreenSharingButton.defaultProps = {
  attachLocalFeed: () => {},
};
