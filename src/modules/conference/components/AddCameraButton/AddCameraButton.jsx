import React from 'react';
import Button from '@material-ui/core/Button';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Popper from '@material-ui/core/Popper';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import { useStyles } from './AddCameraButton-styles';

const AddCameraButton = ({ attachLocalFeed, devices }) => {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  const classes = useStyles();

  const handleMenuItemClick = (event, device) => {
    attachLocalFeed(device.label.substring(0, 10), {
      media: {
        video: {
          deviceId: device.deviceId,
        },

      },
    });
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };
  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <div ref={anchorRef}>
      <Button
        color="primary"
        variant="contained"
        size="small"
        className={classes.toolbarButton}
        aria-owns={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
      >
    Add camera
        <ArrowDropDownIcon className={classes.toolbarIcon} />
      </Button>
      <Popper open={open} anchorEl={anchorRef.current} transition disablePortal>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper id="menu-list-grow">
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList>
                  {devices.map((option) => (
                    <MenuItem
                      key={option.deviceId}
                      onClick={(event) => handleMenuItemClick(event, option)}
                    >
                      {option.label}
                    </MenuItem>
                  ))}

                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
};

AddCameraButton.propTypes = {
  classes: PropTypes.any,
  onClick: PropTypes.any,
};
export default AddCameraButton;
