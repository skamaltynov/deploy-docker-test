import { createStyles, makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles((theme) => createStyles({
  toolbarButton: {
    marginRight: 10,
    minHeight: '36px',
  },
  toolbarIcon: {
    minHeight: '28px',
    fill: theme.palette.common.white,
  },
}));
