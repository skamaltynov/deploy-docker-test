import React from 'react';
import Button from '@material-ui/core/Button';
import FiberManualRecord from '@material-ui/icons/FiberManualRecord';

import { useStyles } from './RecordButton-styles';

const RecordButton = ({ configureRoom, isRecord }) => {
  const anchorRef = React.useRef(null);
  const blinkCssClass = isRecord ? 'Blink' : '';
  const classes = useStyles();
  const handleToggle = () => {
    configureRoom({
      record: !isRecord,
    });
  };

  return (
    <div ref={anchorRef}>
      <Button
        color="primary"
        variant="contained"
        size="small"
        className={classes.toolbarButton}
        aria-haspopup="true"
        onClick={handleToggle}
      >
        <div className={blinkCssClass} style={{ display: 'flex' }}>
          <FiberManualRecord className={classes.toolbarIcon} />
        </div>
        {isRecord && 'recording'}
        {!isRecord && 'start rec'}
      </Button>
    </div>
  );
};

export default RecordButton;
