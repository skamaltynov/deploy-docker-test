import Button from '@material-ui/core/Button';
import ChatIcon from '@material-ui/icons/Chat';
import PropTypes from 'prop-types';
import React from 'react';

export function ToggleChatButton(props) {
  return (
    <Button
      className={props.classes.chatButton}
      color="primary"
      variant="contained"
      onClick={props.onClick}
    >
    Chat
      <ChatIcon className={props.classes.chatIcon} />
    </Button>
  );
}

ToggleChatButton.propTypes = {
  classes: PropTypes.any,
  onClick: PropTypes.any,
};
