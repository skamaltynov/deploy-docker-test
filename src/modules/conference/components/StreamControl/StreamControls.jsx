import React from 'react';
import _ from 'lodash';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import VideoCam from '@material-ui/icons/Videocam';
import Mic from '@material-ui/icons/Mic';
import MicOff from '@material-ui/icons/MicOff';
import VideoCamOff from '@material-ui/icons/VideocamOff';
import Divider from '@material-ui/core/Divider';
import theme from './theme.css';

export default function StreamControls({
  feed, videoDevices, replaceLocalFeed, detachLocalFeed, feedOptionToggle,
}) {
  const [open, setOpen] = React.useState(false);
  const [cam, setCam] = React.useState(true);
  const [mic, setMic] = React.useState(true);
  const anchorRef = React.useRef(null);

  const handleDeviceClick = (event, device) => {
    replaceLocalFeed(feed.feedName, {
      offer: {
        media: {
          video: {
            deviceId: device.deviceId,
          },

        },
      },
    });
    setOpen(false);
  };

  const handleFullScreenToggleClick = () => {
    feedOptionToggle(feed.feedName, 'fullscreen');
    setOpen(false);
  };

  // const handlePipToggleClick = (event) => {
  //   // event.stopPropagation();
  //   feedOptionToggle(feed.feedName, 'pip')
  //   setOpen(false)
  //
  // }

  const handlePreviewClick = () => {
    feedOptionToggle(feed.feedName, 'preview');
    setOpen(false);
  };

  const handleRemoveClick = () => {
    detachLocalFeed(feed.feedName);
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleToggleVideo = () => {
    if (cam) {
      feed.plugin.muteVideo();
    } else {
      feed.plugin.unmuteVideo();
    }
    setCam((prevOpen) => !prevOpen);
  };

  const handleToggleAudio = () => {
    if (mic) {
      feed.plugin.muteAudio();
    } else {
      feed.plugin.unmuteAudio();
    }
    setMic((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const deviceId = _.get(feed, 'constraints.deviceId');

  return (
    <>

      <ButtonGroup
        className={theme.toolbarButton}
        variant="contained"
        color="primary"
        ref={anchorRef}
        aria-label="split button"
      >
        {(feed.feedName !== 'mouthwatch' && feed.feedName !== 'screenshare') && (
        <Button onClick={handleToggleVideo}>
          {cam ? (
            <VideoCamOff />
          ) : (
            <VideoCam />
          )}
        </Button>
        )}
        {(feed.feedName !== 'mouthwatch' && feed.feedName !== 'screenshare') && (
        <Button onClick={handleToggleAudio}>
          {mic ? (
            <MicOff />
          ) : (
            <Mic />
          )}

        </Button>
        )}
        <Button
          color="primary"
          variant="contained"
          size="small"
          aria-owns={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
        >
          {feed.feedName}
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper open={open} anchorEl={anchorRef.current}>

            <Paper id="menu-list-grow">
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList>
                  <MenuItem
                    key="fullscreen"
                    onClick={(event) => handleFullScreenToggleClick(event)}
                  >
                    Toggle Fullscreen
                  </MenuItem>
                  {/* <MenuItem */}
                  {/*  key={'pip'} */}
                  {/*  onClick={event => handlePipToggleClick(event)} */}
                  {/* > */}
                  {/*  Picture in picture */}
                  {/* </MenuItem> */}
                  <MenuItem
                    key="preview"
                    onClick={(event) => handlePreviewClick(event)}
                  >
                    Toggle preview
                  </MenuItem>
                  <MenuItem
                    key="remove"
                    onClick={(event) => handleRemoveClick(event)}
                  >
                    Remove
                  </MenuItem>
                  {(feed.feedName !== 'mouthwatch' && feed.feedName !== 'screenshare') && (
                    <>
                      <Divider />
                      {videoDevices.filter((device) => device.deviceId !== deviceId).map((option) => (
                        <MenuItem
                          key={option.deviceId}
                          selected={deviceId === option.deviceId}
                          onClick={(event) => handleDeviceClick(event, option)}
                        >
                          {option.label}
                        </MenuItem>
                      ))}
                    </>
                  )}


                </MenuList>
              </ClickAwayListener>
            </Paper>

      </Popper>
    </>
  );
}
