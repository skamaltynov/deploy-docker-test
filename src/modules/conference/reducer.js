import { SET_PROVIDER_TO_PROVIDER } from 'modules/messages/actions/actionTypes';
import { omit } from 'lodash';
import * as at from './actions/actionTypes';
import { getQueryStringValue, isMobile } from '../../utils/utils';

const { hostname } = window.location;

let server = `https://${hostname}:8088/janus`;

if (hostname === 'localhost') {
  server = `http://${hostname}:8088/janus`;
}
const videoCodec = isMobile() ? 'h264' : 'vp8';


const initialState = {
  iceServers: [],
  isProvider: Boolean(getQueryStringValue('isProvider')),

  videoDevices: [],
  audioDevices: [],

  localDevices: {},
  mouthWatchDevices: {},
  isRecord: false,
  feeds: {},
  mcu: {},
  currentUser: {
    id: new Date().getTime(),
    name: null,
  },
  janusConfig: {
    janus: { server },
    room: {
      request: 'create',
      permanent: false,
      transport_wide_cc_ext: true,
      notify_joining: true,
      fir_freq: 2,
      videocodec: videoCodec,
      secret: 'test',
      publishers: 60,
      description: 'Video room',
      bitrate: 0,
      record: false,
    },
    debug: true
  },
  notification: {},
  pip: false,
  providerToProvider: false
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  const { feeds } = action;

  switch (type) {
    case at.SET_VIDEO_DEVICES:
      return {
        ...state,
        videoDevices: payload,
      };

    case at.SET_AUDIO_DEVICES:
      return {
        ...state,
        audioDevices: payload,
      };

    case at.SET_LOCAL_DEVICES:
      return {
        ...state,
        localDevices: payload,
      };

    case at.SELECT_MOUTH_WATCH_DEVICES:
      return {
        ...state,
        mouthWatchDevices: payload,
      };

    case at.ROOM_LOCAL_FEED:
    case at.ROOM_REMOTE_FEED:
      return {
        ...state,
        feeds: {
          ...state.feeds,
          [payload.feed.id]: payload.feed
        },

      };

    case at.ROOM_LOCAL_STREAM:
    case at.ROOM_REMOTE_STREAM:
      return {
        ...state,
        feeds: {
          ...state.feeds,
          [payload.feed.id]: {
            ...state.feeds[payload.feed.id],
            status: payload.status,
            stream: payload.stream
          }
        },
      };
    case at.ROOM_REMOTE_UPDATE_STATS:
      return {
        ...state,
        feeds: {
          ...state.feeds,
          [payload.feedId]: {
            ...state.feeds[payload.feedId],
            stats: payload.stats,
          }
        },
      };
    case at.ROOM_REMOVE_FEED:

      return {
        ...state,
        feeds: omit(state.feeds, payload.id)
      };

    case at.AUDIO_DISABLED:
      return { ...state, audioDisabled: true };
    case at.ATTACH_MCU_ERROR:
    case at.ROOM_EXISTS_ERROR:
    case at.CREATE_ROOM_ERROR:
    case at.ROOM_LOCAL_FEED_ERROR:
    case at.ROOM_REMOTE_FEED_ERROR:
    case at.ROOM_ICE_ERROR:
    case at.ROOM_LOCAL_DATA_ERROR:
      return { ...state, error: action };

    case at.CREATE_SESSION:
      const { config: { roomId } } = action;

      return {
        ...state,
        janusConfig: {
          ...state.janusConfig,
          room: {
            ...state.janusConfig.room,
            room: roomId
          },

        }
      };

    case at.DESTROY_SESSION:

      return {
        ...state,
        janusConfig: {},
        mcu: {
          janus: null,
          connected: false,
        },
      };
    case at.CREATE_SESSION_SUCCESS:
      return {
        ...state,
        janusConfig: {
          ...state.janusConfig,
        },
        mcu: {
          janus: payload.janus,
          connected: true,
        },
      };
    case at.CLEAN_FEEDS:
      return {
        ...state,
        feeds: []
      };
    case at.CREATE_SESSION_ERROR:
      return {
        ...state,
        mcu: {
          error: action,
          connected: false,
        },
      };
    case at.FEED_OPTION_TOGGLE_CHANGE:
      return {
        ...state,
        feeds,
      };
    case at.SET_NOTIFICATION:
      return {
        ...state,
        notification: {
          type: payload.type,
          message: payload.message,
        },
      };
    case at.TOGGLE_PIP:
      return {
        ...state,
        pip: payload.pip,
      };
    case at.RESET_NOTIFICATION:
      return {
        ...state,
        notification: {},
      };
    case at.SET_USER_INFO:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          GUID: payload.id,
          name: payload.username
        }
      };
    case SET_PROVIDER_TO_PROVIDER:
      return {
        ...state,
        providerToProvider: payload,
      };

    case at.ROOM_CONFIGURE:
      return {
        ...state,
        janusConfig: {
          ...state.janusConfig,
          room: {
            ...state.janusConfig.room,
            ...payload.config
          },

        }
      };
    default:
      return state;
  }
};
