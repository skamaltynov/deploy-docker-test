import * as at from './actionTypes';

export const fetchConferenceDetails = (id) => ({
  type: at.FETCH_CONFERENCE_DETAILS,
  payload: id
});

export const setPhotosList = (photosList) => ({
  type: at.SET_PHOTOS_LIST,
  payload: photosList
});

export const setVideoList = (videosList) => ({
  type: at.SET_VIDEOS_LIST,
  payload: videosList
});

export const loadMessages = (messages) => ({
  type: at.LOAD_MESSAGES,
  payload: messages
});

export const setLoading = (loading) => ({
  type: at.SET_LOADING,
  payload: loading,
});
