import { get, uniqBy } from 'lodash';

export const photosListSelector = ({ pastConference: state }) => get(state, 'photosList', []);
export const videosListSelector = ({ pastConference: state }) => get(state, 'videosList', []);
export const tokenSelector = (state) => get(state, 'token.token.teledentApiToken', '');
export const userSelector = (state) => get(state, 'token.token', {});
export const messagesSelector = (state) => uniqBy(state.chat.messages, 'timeStamp');
export const loadingSelector = ({ pastConference: state }) => get(state, 'loading', false);
