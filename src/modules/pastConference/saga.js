import { call, put, takeEvery } from 'redux-saga/effects';
import * as at from './actions/actionTypes';
import * as a from './actions';
import APImethods from '../../api';

function* fetchConferenceDetailsSaga({ payload: id }) {
  yield put(a.setLoading(true));

  try {
    const { data } = yield call([APImethods, 'get'], `appointments/${id}/media`);
    const photosList = data.filter(({ mime_type }) => mime_type === 'image/png');
    const videosList = data.filter(({ mime_type }) => mime_type === 'video/webm');


    yield put(a.setPhotosList(photosList));
    yield put(a.setVideoList(videosList));

    const params = {
      appointmentId: id
    };

    const { data: chat } = yield call([APImethods, 'get'], '/notes/getnotes', params);

    const messages = yield chat
      .map(({ body }) => {
        try {
          return JSON.parse(body);
        } catch (e) {
          // eslint-disable-next-line
						console.log(e);
          return false;
        }
      })
      .filter((item) => item);

    yield put(a.loadMessages(messages));
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  } finally {
    yield put(a.setLoading(false));
  }
}

export default function* pastConferenceSaga() {
  yield takeEvery(at.FETCH_CONFERENCE_DETAILS, fetchConferenceDetailsSaga);
}
