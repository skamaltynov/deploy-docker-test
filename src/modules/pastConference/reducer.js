import * as at from './actions/actionTypes';

const initialState = {
  photosList: [],
  videosList: [],
  messages: [],
  loading: false
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case at.SET_PHOTOS_LIST:
      return {
        ...state,
        photosList: payload
      };

    case at.SET_VIDEOS_LIST:
      return {
        ...state,
        videosList: payload
      };

    case at.LOAD_MESSAGES:
      return {
        ...state,
        messages: payload
      };

    case at.SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    default:
      return state;
  }
};
