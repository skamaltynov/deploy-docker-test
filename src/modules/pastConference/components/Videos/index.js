import { connect } from 'react-redux';
import Videos from './Videos';
import { tokenSelector, videosListSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  videosList: videosListSelector(state),
  token: tokenSelector(state)
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Videos);
