import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Typography from '@material-ui/core/Typography';
import ReactPlayer from 'react-player';
import theme from './Videos.module.css';

class Photos extends Component {
	static propTypes = {
	  videosList: PropTypes.array.isRequired
	};

	// state = {
	// 	playing: false
	// };
	//
	// handlePlay = () => {
	// 	this.setState({ playing: true })
	// };
	//
	// handlePause = () => {
	// 	this.setState({ playing: false })
	// };

	src = (url) => {
	  const { token } = this.props;

	  return `${url}?authToken=${token}`;
	};

	render() {
	  const { videosList } = this.props;
	  // const { playing } = this.state;

	  return (
  <div className={theme.container}>
    <Typography variant="h4">Videos</Typography>

    <GridList className={theme.gridList}>
      {videosList.map(({ id, url }) => (
        <GridListTile key={id} className={theme.imgFullHeight}>
          <ReactPlayer
            className="react-player"
            url={[{ src: this.src(url), type: 'video/webm' }]}
            controls
            width="100%"
            height="100%"
          />
        </GridListTile>
      ))}
    </GridList>
  </div>
	  );
	}
}

export default Photos;
