import React, { Component } from 'react';
import ChatBody from 'modules/chat/components/ChatBody';
import Typography from '@material-ui/core/Typography';
import Loading from 'components/Loading';
import theme from './PastConference.module.css';
import Photos from '../Photos';
import Videos from '../Videos';

class PastConference extends Component {
  componentDidMount() {
    const { onConferenceDetailsFetch, match: { params: { appointmentId } } } = this.props;

    onConferenceDetailsFetch(appointmentId);
  }

  render() {
    const { user, messages, loading } = this.props;

    if (loading) return <Loading />;

    return (
      <div className={theme.container}>
        <div className={theme.media}>
          <Photos />

          <Videos />
        </div>

        <div className={theme.chatContainer}>
          <Typography variant="h4">Chat history</Typography>

          <ChatBody theme={{ body: theme.chatBodyContainer }} user={user} messages={messages} />
        </div>
      </div>
    );
  }
}

export default PastConference;
