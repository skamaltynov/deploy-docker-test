import { connect } from 'react-redux';
import PastConference from './PastConference';
import { fetchConferenceDetails } from '../../actions';
import { loadingSelector, messagesSelector, userSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  messages: messagesSelector(state),
  user: userSelector(state),
  loading: loadingSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  onConferenceDetailsFetch: (id) => dispatch(fetchConferenceDetails(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(PastConference);
