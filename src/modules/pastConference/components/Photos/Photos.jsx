import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Typography from '@material-ui/core/Typography';
import theme from './Photos.module.css';

class Photos extends Component {
	static propTypes = {
	  photosList: PropTypes.array.isRequired
	};

	src = (url) => {
	  const { token } = this.props;

	  return `${url}?authToken=${token}`;
	};

	render() {
	  const { photosList } = this.props;

	  return (
  <div className={theme.container}>
    <Typography variant="h4">Photos</Typography>

    <GridList className={theme.gridList}>
      {photosList.map(({ id, url }) => (
        <GridListTile key={id} className={theme.imgFullHeight}>
          <img src={this.src(url)} alt="" />
        </GridListTile>
      ))}
    </GridList>
  </div>
	  );
	}
}

export default Photos;
