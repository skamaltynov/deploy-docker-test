import { connect } from 'react-redux';
import Photos from './Photos';
import { photosListSelector, tokenSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  photosList: photosListSelector(state),
  token: tokenSelector(state)
});

const mapDispatchToProps = () => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Photos);
