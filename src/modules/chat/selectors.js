import { get, uniqBy } from 'lodash';
import { createSelector } from 'reselect';
import { localFeedsSelector } from '../conference/selectors/videoRoom';


export const messagesSelector = (state) => uniqBy(state.chat.messages, 'timeStamp');
export const appointmentIdSelector = () => {
  const pathToArray = window.location.pathname.split('/');

  return pathToArray[pathToArray.length - 1];
};
export const feedsSelector = ({ conference: state }) => get(state, 'feeds', []);

export const userSelector = createSelector(
  localFeedsSelector,
  (feeds) => get(feeds, '0.user')
);
