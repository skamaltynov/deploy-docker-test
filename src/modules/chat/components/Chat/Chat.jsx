import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, isEqual } from 'lodash';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import ChatBody from '../ChatBody';
import theme from './Chat.module.css';

class Chat extends Component {
    static propTypes = {
      onMessageSend: PropTypes.func.isRequired,
      messages: PropTypes.array.isRequired,
      user: PropTypes.object.isRequired,
      onChatClose: PropTypes.func.isRequired,
      appointmentId: PropTypes.string.isRequired,
    };

    constructor(props) {
      super(props);

      this.chatBody = React.createRef();
    }

    componentDidMount() {
      const { loadChat, appointmentId, messages } = this.props;

      if (isEmpty(messages)) loadChat(appointmentId);
      this.scrollBottom();
    }

    componentDidUpdate(prevProps) {
      const { messages } = this.props;

      if (!isEqual(prevProps.messages, messages)) this.scrollBottom();
    }

    sendMessageHandler = (event) => {
      const { onMessageSend, user, appointmentId } = this.props;
      const { value } = this.input;

      if (!value) return;

      if (event.keyCode === 13) {
        const messageData = {
          message: value,
          timeStamp: Date.now(),
          user,
        };

        onMessageSend(messageData, appointmentId);
        this.input.value = '';
      }
    };

    scrollBottom = () => {
      this.chatBody.current.scrollTop = this.chatBody.current.scrollHeight;
    };

    render() {
      const { messages, onChatClose, user } = this.props;

      return (
        <ClickAwayListener onClickAway={onChatClose}>
          <Paper>
            <Box className={theme.container}>
              <AppBar position="static" className={theme.header}>
                <Toolbar>
                  <Typography variant="h6">Chat</Typography>
                </Toolbar>
              </AppBar>

              <ChatBody messages={messages} user={user} ref={this.chatBody} />

              <Box className={theme.textComposer}>
                <TextField
                  placeholder="Write a message..."
                  fullWidth
                  margin="normal"
                  variant="filled"
                  InputLabelProps={{ shrink: true }}
                  inputRef={(input) => { this.input = input; }}
                  onKeyDown={this.sendMessageHandler}
                  className={theme.textField}
                />
              </Box>
            </Box>
          </Paper>

        </ClickAwayListener>
      );
    }
}

export default Chat;
