import { connect } from 'react-redux';
import Chat from './Chat';
import { loadChat, sendMessage } from '../../actions';
import { appointmentIdSelector, messagesSelector, userSelector } from '../../selectors';

const mapStateToProps = (state) => ({
  messages: messagesSelector(state),
  user: userSelector(state),
  appointmentId: appointmentIdSelector()
});

const mapDispatchToProps = (dispatch) => ({
  onMessageSend: (message, appointmentId) => dispatch(sendMessage(message, appointmentId)),
  loadChat: (appointmentId) => dispatch(loadChat(appointmentId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
