import { themr } from 'react-css-themr';
import ChatBody from './ChatBody';
import theme from './ChatBody.module.css';

export default themr('ChatBody', theme)(ChatBody);
