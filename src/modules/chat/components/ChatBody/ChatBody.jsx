import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import moment from 'moment';
import { getUserFromDisplay } from '../../../conference/saga';

class ChatBody extends Component {
	isRemoteUser = (messageUser) => {
	  const { user } = this.props;

	  return user.id !== messageUser.id;
	};

	userName = (user) => {
	  const { name } = getUserFromDisplay(user.display);

	  return name;
	};

	time = (timeStamp) => moment(timeStamp).format('HH:mm');

	render() {
	  const { messages, theme } = this.props;

	  return (
  <Box className={theme.body} ref={this.chatBody}>
    {messages.map(({ message, timeStamp, user }) => (
      <div
        className={theme.message}
        key={timeStamp}
        style={{ alignItems: !this.isRemoteUser(user) ? 'flex-end' : '' }}
      >
        <div className={theme.messageHeader}>
          {this.isRemoteUser(user) && <div className={theme.author}>{`${this.userName(user)},`}</div>}
          <div className={theme.date}>{this.time(timeStamp)}</div>
        </div>
        <div className={theme.messageBody}>{message}</div>
      </div>
    ))}
  </Box>
	  );
	}
}

export default ChatBody;
