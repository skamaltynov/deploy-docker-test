import { call, put, select, takeEvery } from '@redux-saga/core/effects';
import * as at from './actions/actionTypes';
import { loadLocalData, loadLocalError, loadMessages } from './actions';
import APImethods from '../../api';
import { dataFeedSelector } from '../imageCapture/selectors';
import { sendDataChannelMessageAsync } from '../conference/actions/janusAsyncActions';

function* loadChat({ payload }) {
  const { data: chat } = yield call([APImethods, 'get'], `/notes/videoSession/${payload}`);

  const messages = yield chat
    .map(({ body }) => {
      try {
        return JSON.parse(body);
      } catch (e) {
        // eslint-disable-next-line
        console.log(e);
        return false;
      }
    })
    .filter((item) => item);

  yield put(loadMessages(messages));
}

function* sendMessage({ payload: { appointmentId, messageData } }) {
  const { body } = messageData;
  const feed = yield select(dataFeedSelector);

  try {
    yield call(sendDataChannelMessageAsync, feed, JSON.parse(body));
    yield call([APImethods, 'post'], `notes/videoSession/${appointmentId}`, messageData);
  } catch (e) {
    yield put(loadLocalError(e));
  }
  yield put(loadLocalData(JSON.parse(body)));
}

export default function* chatSaga() {
  yield takeEvery(at.LOAD_CHAT, loadChat);
  yield takeEvery(at.SEND_MESSAGE, sendMessage);
}
