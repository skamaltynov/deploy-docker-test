import * as at from './actions/actionTypes';

const initialState = {
  messages: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case at.ROOM_LOCAL_DATA:
      return {
        ...state,
        messages: [...state.messages, payload],
      };

    case at.ROOM_DATA_CHAT:
      return {
        ...state,
        messages: [...state.messages, payload],
      };

    case at.LOAD_MESSAGES:
      return {
        ...state,
        messages: payload,
      };

    default:
      return state;
  }
};
