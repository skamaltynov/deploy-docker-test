import * as at from './actionTypes';

export const sendMessage = (data, appointmentId) => {
  data = data || null;
  const text = JSON.stringify(data);

  const messageData = {
    title: 'title',
    body: text
  };

  return {
    type: at.SEND_MESSAGE,
    payload: { messageData, appointmentId }
  };
};


export const loadLocalData = (data) => ({
  type: at.ROOM_LOCAL_DATA,
  payload: data
});

export const loadLocalError = (message) => ({
  type: at.ROOM_LOCAL_DATA_ERROR,
  payload: message,
});

export const loadChat = (appointmentId) => ({
  type: at.LOAD_CHAT,
  payload: appointmentId
});

export const loadMessages = (messages) => ({
  type: at.LOAD_MESSAGES,
  payload: messages
});
