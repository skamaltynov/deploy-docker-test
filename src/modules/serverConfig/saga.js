import { call, put, takeLatest } from '@redux-saga/core/effects';
import { storeConfigs } from './actions';
import { LOAD_CONFIG } from './actions/actionTypes';
import APImethods from '../../api';

function* loadConfig() {
  try {
    const { data } = yield call([APImethods, 'get'], '/configuration/roomSettings/');

    yield put(storeConfigs({ config: data }));
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  }
}

export default function* serverConfigSaga() {
  yield takeLatest(LOAD_CONFIG, loadConfig);
}
