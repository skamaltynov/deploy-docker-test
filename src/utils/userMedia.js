export default {
  async getMediaDevices() {
    try {
      return await navigator.mediaDevices.enumerateDevices();
    } catch (e) {
      // eslint-disable-next-line
      console.log(`Error: ${e}`);
      return null;
    }
  },
  async getVideoDevices() {
    try {
      const devices = await this.getMediaDevices();

      return devices
        .filter((item) => item.kind === 'videoinput')
        .map((item) => {
          const { label, deviceId } = item;

          return { label, deviceId };
        });
    } catch (e) {
      // eslint-disable-next-line
      console.log(`Error: ${e}`);
      return null;
    }
  },
  async getAudioDevices() {
    try {
      const devices = await this.getMediaDevices();

      return devices
        .filter((item) => item.kind === 'audioinput')
        .map((item) => {
          const { label, deviceId } = item;

          return { label, deviceId };
        });
    } catch (e) {
      // eslint-disable-next-line
      console.log(`Error: ${e}`);
      return null;
    }
  },
};
