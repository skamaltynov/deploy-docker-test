import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import theme from './Modal.module.css';

export default class TransitionsModal extends Component {
	static propTypes = {
	  children: PropTypes.node.isRequired,
	  open: PropTypes.bool.isRequired,
	  onClose: PropTypes.func,
	  onConfirm: PropTypes.func,
	  confirmTitle: PropTypes.string.isRequired,
	  cancelTitle: PropTypes.string.isRequired
	};

	render() {
	  const { children, open, onClose, onConfirm, confirmTitle, cancelTitle } = this.props;

	  return (
			<div className={theme.container}>
				<Modal
					aria-labelledby="transition-modal-title"
					aria-describedby="transition-modal-description"
					className={theme.modal}
					open={open}
					onClose={onClose}
					closeAfterTransition
					BackdropComponent={Backdrop}
					BackdropProps={{
					  timeout: 500,
					}}
				>
					<Fade in={open}>
						<div className={theme.childContainer}>
							{children}
							<div className={theme.buttonsGroup}>
								{onClose
								&& (
										<Button
												variant="contained"
												color="secondary"
												className={theme.button}
												onClick={onClose}
										>
											{cancelTitle}
										</Button>
								)}
								{onConfirm
										&& (
										<Button
												variant="contained"
												color="primary"
												className={theme.button}
												onClick={onConfirm}
										>
											{confirmTitle}
										</Button>
										)}
							</div>
						</div>
					</Fade>
				</Modal>
			</div>
	  );
	}
}
