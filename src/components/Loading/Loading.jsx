import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import theme from './Loading.module.css';

class Loading extends Component {
  render() {
    return (
      <div className={theme.container}>
        <CircularProgress disableShrink />
      </div>
    );
  }
}

export default Loading;
