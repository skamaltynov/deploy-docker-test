import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import theme from './Menu.module.css';

class MenuComponent extends Component {
	static propTypes = {
	  anchorEl: PropTypes.any,
	  onClose: PropTypes.func.isRequired,
	  onShowCreateMessage: PropTypes.func.isRequired
	};

	handleCreateMessage = () => {
	  const { onShowCreateMessage, onClose } = this.props;

	  onShowCreateMessage();
	  onClose();
	};

	render() {
	  const { anchorEl, onClose } = this.props;

	  return (
				<div className={theme.container}>
					<Paper className={theme.paper}>
						<Menu
								id="simple-menu"
								anchorEl={anchorEl}
								keepMounted
								open={Boolean(anchorEl)}
								onClose={onClose}
						>
							<MenuItem>Patient</MenuItem>
							<MenuItem>Appointment</MenuItem>
							<MenuItem>Task</MenuItem>
							<MenuItem onClick={this.handleCreateMessage}>Create Message</MenuItem>
						</Menu>
					</Paper>
				</div>
	  );
	}
}

export default MenuComponent;
